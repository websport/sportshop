<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.pnv.model.StaffInformation"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="en">

<head>
<meta charset="utf-8">
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Accord, premium HTML5 &amp; CSS3 template</title>

<!-- Favicons Icon -->
<link rel="icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />
<link rel="shortcut icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />

<!-- Mobile Specific -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">


<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/bootstrap.min.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/slider.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/owl.carousel.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/owl.theme.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/font-awesome.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/style.css" type="text/css">

<!-- Google Fonts -->

</head>

<body class="cms-index-index">
	<div class="page">
		<!-- Header -->
		<header class="header-container">
			<div class="header-top">
				<div class="container">
					<div class="row">
						<div class="col-lg-5 col-md-5 col-xs-6">

							<div class="welcome-msg hidden-xs">
								<h2>Welcome to SD Shop</h2>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-xs-6">

							<!-- Header Top Links -->
							<div class="toplinks">
								<div class="links">

									</br>


									<div class="links">
										<%@include file="message.jsp"%>
										<form class="form-signin"
											action="<%=request.getContextPath()%>/userController"
											method="GET">
											<button type="submit" class="button btn-cart" name="formAction"
												value="register"><b>REGISTER</b></button>
											</a>
											<button type="submit" class="button btn-cart" name="formAction"
												value="show"><B>SHOW ACCOUNT</B></button>
											</a>
											<button type="submit" class="button btn-cart" name="formAction"
												value="changePassword"><b>CHANGE PASSWORD</b></button>
										</form>
											<form class="form-signin"
											action="<%=request.getContextPath()%>/productController"
											method="GET">
											<button type="submit" class="button btn-cart" value="insertProduct"
												name="formAction"><b>INSERT PRODUCT</b></button>
										</form>
										<form class="form-signin"
											action="<%=request.getContextPath()%>/productController"
											method="GET">
											<button type="submit" class="button btn-cart" value="showProduct"
												name="formAction"><b>SHOW PRODUCT</b></button>
										</form>
										<form class="form-signin"
											action="<%=request.getContextPath()%>/productController"
											method="POST">
										<button type="submit" class="button ctn-cart" value="order" name="formAction"><b>ORDER</b></button>
										</form>
										<form class="form-signin" action="userController"
											method="POST">
											<button type="submit" class="button btn-cart" value="logout"
												name="action"><b>LOGOUT</b></button>
											</a>
										</form>
									
									</div>

								</div>
							</div>
							<!-- End Header Top Links -->
						</div>
					</div>
				</div>
			</div>

			<div class="header container">
				<div class="row">
					<div class="col-lg-3 col-sm-4 col-md-3">
						<!-- Header Logo -->
						<div class="logo">
							<a title="Magento Commerce" href=""><img
								alt="Magento Commerce"
								src="<%=request.getContextPath()%>/images/image_SDshop.png"></a>
						</div>
						<!-- End Header Logo -->
					</div>
				</div>
			</div>
		</header>
		<!-- end header -->
		<!-- Navbar-->
		<%@include file="menu.jsp"%>
		<%@include file="slide.jsp"%>
		<section class="main-container col1-layout home-content-container">
			<div class="container">
				<div class="row">
					<div class="std">
						<div class="best-pro col-lg-12">
							<div class="slider-items-products">
								<div class="new_title center">
									<h2>Club Clothes</h2>
								</div>
								<div id="best-seller-slider"
									class="product-flexslider hidden-buttons">
									<div class="slider-items slider-width-col4">
										<c:forEach var="employee" items="${employeeList}" begin="1"
											end="5">
											<!-- Item -->
											<div class="item">
												<div class="col-item">
													<div class="item-inner">
														<div class="item-img">
															<div class="item-img-info">
																<a
																	href="<%=request.getContextPath()%>/productController?formAction=showInforProduct&id=${employee.productId}"
																	title="Sample Product" class="product-image"> <img
																	src="<%=request.getContextPath() %>/images/${employee.image}"
																	alt="Sample Product">
																</a>
																<div class="item-box-hover">
																	<div class="box-inner">
																		<div class="product-detail-bnt">
																			<a
																				href="<%=request.getContextPath()%>/productController?formAction=showInforProduct&id=${employee.productId}"
																				class="button detail-bnt"> <span> Quick View</span></a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="item-info">
															<div class="info-inner">
																<div class="item-title">
																	<a href="product_detail.html">${employee.productName}
																	</a>
																</div>
																<div class="item-content">
																	<div class="rating">
																		<div class="ratings">
																			<div class="rating-box">
																				<div class="rating" style="width: 80%"></div>
																			</div>
																			<p class="rating-links">
																				<a href="#">1 Review(s)</a> <span class="separator">|</span>
																				<a href="#">Add Review</a>
																			</p>
																		</div>
																	</div>
																	<div class="item-price">
																		<div class="price-box">
																			<span class="regular-price" id="product-price-1">
																				<span class="price">${employee.price}00 VND</span>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
															<div class="actions">
																<span class="add-to-links"> <a
																	href="wishlist.html" class="link-wishlist"
																	title="Add to Wishlist"><span>Add to
																			Wishlist</span></a>
																	<button title="Add to Cart" class="button btn-cart">
																		<span>Add to Cart</span>
																	</button> <a href="compare.html" class="link-compare"
																	title="Add to Compare"><span>Add to Compare</span></a></span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<!-- End Item -->
										</c:forEach>
									</div>
								</div>
							</div>
						</div>
						<div class="featured-pro col-lg-12">
							<div class="slider-items-products">
								<div class="new_title center">
									<h2>National Clothes</h2>
								</div>
								<div id="featured-slider"
									class="product-flexslider hidden-buttons">
									<div class="slider-items slider-width-col4">
										<c:forEach var="product" items="${product}" begin="1" end="5">
											<!-- Item -->
											<div class="item">
												<div class="col-item">
													<div class="item-inner">
														<div class="item-img">
															<div class="item-img-info">
																<a
																	href="<%=request.getContextPath()%>/productController?formAction=showInforProduct&id=${product.productId}"
																	title="Sample Product" class="product-image"> <img
																	src="<%=request.getContextPath() %>/images/${product.image}"
																	alt="Sample Product">
																</a>
																<div class="item-box-hover">
																	<div class="box-inner">
																		<div class="product-detail-bnt">
																			<a
																				href="<%=request.getContextPath()%>/productController?formAction=showInforProduct&id=${product.productId}"
																				class="button detail-bnt"><span>Quick
																					View</span></a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="item-info">
															<div class="info-inner">
																<div class="item-title">
																	<a href="grid.html" title="Sample Product">
																		${product.productName} </a>
																</div>
																<div class="item-content">
																	<div class="rating">
																		<div class="ratings">
																			<div class="rating-box">
																				<div class="rating" style="width: 80%"></div>
																			</div>
																			<p class="rating-links">
																				<a href="#">1 Review(s)</a> <span class="separator">|</span>
																				<a href="#">Add Review</a>
																			</p>
																		</div>
																	</div>
																	<div class="item-price">
																		<div class="price-box">
																			<span class="regular-price"> <span
																				class="price">${product.price}00 VND</span>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
															<div class="actions">
																<span class="add-to-links"> <a
																	href="wishlist.html" class="link-wishlist"
																	title="Add to Wishlist"><span>Add to
																			Wishlist</span></a>
																	<button title="Add to Cart" class="button btn-cart">
																		<span>Add to Cart</span>
																	</button> <a href="compare.html" class="link-compare"
																	title="Add to Compare"><span>Add to Compare</span></a></span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<!-- End Item -->
										</c:forEach>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End main container -->

		<!-- Latest Blog -->
		<section class="latest-blog wow bounceInDown animated">
			<div class="container">
				<div class="row">
					<div class="new_title center">
						<h2>
							<span>Shoes and Ball</span>
						</h2>
					</div>
					<c:forEach var="products" items="${products}" begin="1" end="4">
						<div class="col-xs-12 col-sm-3">
							<div class="blog_inner">
								<div class="blog-img blog-l">
									<img
										src="<%=request.getContextPath() %>/images/${products.image}"
										alt="Blog image">
									<div class="mask">
										<a class="info"
											href="<%=request.getContextPath()%>/productController?formAction=showInforProduct&id=${products.productId}">Quick View
										</a>
									</div>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
		</section>
		</table>



		<!-- Footer -->
		<%@include file="footer.jsp"%>
		<!-- End Footer -->

	</div>


	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/parallax.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/common.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/slider.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/owl.carousel.min.js"></script>
	<script type="text/javascript">
		//<![CDATA[
		jQuery(function() {
			jQuery(".slideshow").cycle({
				fx : 'scrollHorz',
				easing : 'easeInOutCubic',
				timeout : 10000,
				speedOut : 800,
				speedIn : 800,
				sync : 1,
				pause : 1,
				fit : 0,
				pager : '#home-slides-pager',
				prev : '#home-slides-prev',
				next : '#home-slides-next'
			});
		});
		//]]>
	</script>
</body>

<!-- Giao dien duoc chia se mien phi tai www.ptheme.net [Free HTML Download]. SKYPE[ptheme.net] - EMAIL[ptheme.net@gmail.com].-->
</html>