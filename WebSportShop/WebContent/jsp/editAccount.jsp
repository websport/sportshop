<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>edit Account</title>
<!-- Favicons Icon -->
<link rel="icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />
<link rel="shortcut icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />

<!-- Mobile Specific -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/bootstrap.min.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/slider.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/owl.carousel.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/owl.theme.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/font-awesome.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/style.css" type="text/css">

<!-- Google Fonts -->
<style>
table {
    border-collapse: collapse;
    width: 50%;
    height: 400px;
    font-size: 15px;
}

th, td {
    text-align: center;
    padding: 8px;
}

tr:nth-child(even){background-color:#eacece}
</style>
</head>
<body>
<div class="page">
<%@include file="header1.jsp"%>
<%@include file="menu.jsp"%>
  <center><h3><b>Edit Account</b></h3></center>
 <div class="container">
			<div class="card card-container"> 
	<!-- <form class="form-signin" action="userController" method="GET"> -->
	
	<center>
		<form class="form-signin"
			action="<%=request.getContextPath()%>/userController" method="POST">
			<%@include file="message.jsp"%>
			<table border="3" cellpadding="5" cellspacing="1">
				<tr>
					<td><b>Name</b></td>
					<td><b>${edit.name}</b></td>
				</tr>
				<tr>
					<td><b></>Address</b></td>
					<td><b><input type="text" name="address" value="${edit.address}"  /></b></td>
				</tr>
				<tr>
					<td><b>Phone</b></td>
					<td><b><input type="text" name="phone" value="${edit.phone}" /></b></td>
				</tr>
				<tr>
					<td><b>Gender</b></td>
					<td><b><c:choose>
							<c:when test="${edit.gender eq true}"> Male  <br />

							</c:when>
							<c:otherwise> Female <br />
							</c:otherwise>
						</c:choose></b></td>
				</tr>
				<tr>
					<td><b>Identity Card</b></td>
					<td><b>${edit.identityCard}</b></td>
				</tr>
				<tr>
					<td><b>UserName</b></td>
					<td><b><input type="text" name="userName"
						value="${edit.userName}" /></b></td>
				</tr>
				<tr>
					<td><b>Password</b></td>
					<td><b><input type="password" name="password"
						value="${edit.password}" /></b></td>
				</tr>
				<tr>
					<td><b>Role</b></td>
					<td><b><c:choose>
							<c:when test="${edit.role eq '2'}"> Boss<br />
							</c:when>
							<c:otherwise>Staff<br />
							</c:otherwise>
						</c:choose></b></td>
				</tr>


			</table>
			</br>
			<button type="submit" style="background-color: #3a96f2" class="button btn-cart" name="action" value="edit"><b>EDIT</b></button>
			<input type="hidden" name="idEdit" value="${edit.id}">
		</form>
		<form class="form-signin"
			action="<%=request.getContextPath()%>/userController" method="GET">
			<button type="submit" style="background-color: #3a96f2 " class="button btn-cart" value="Back" onclick="javascript:history.go(-1)"><b>BACK</b></button>
		</form>
	</center>
</div>
</div>
<%@include file="footer.jsp"%>
</div>
</body>
</html>