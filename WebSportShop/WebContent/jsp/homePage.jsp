<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="en">

<head>
<meta charset="utf-8">
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Accord, premium HTML5 &amp; CSS3 template</title>

<!-- Favicons Icon -->
<link rel="icon" href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico" type="image/x-icon" />

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


<link rel="stylesheet" href="<%=request.getContextPath() %>/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/slider.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/owl.theme.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/style.css" type="text/css">

<!-- Google Fonts -->

</head>

<body class="cms-index-index">
<div class="page"> 
  <!-- Header -->
 <%@include file = "header.jsp"%>
  <!-- end header --> 
  <!-- Navbar -->
<%@include file = "menu.jsp"%>
  <%@include file = "slide.jsp"%>
 <%@include file = "container.jsp"%>
 
  
  
  <!-- Footer -->
<%@include file = "footer.jsp"%>
  <!-- End Footer --> 
  
</div>


<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.min.js"></script> 
<script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<%=request.getContextPath() %>/js/parallax.js"></script> 
<script type="text/javascript" src="<%=request.getContextPath() %>/js/common.js"></script> 
<script type="text/javascript" src="<%=request.getContextPath() %>/js/slider.js"></script> 
<script type="text/javascript" src="<%=request.getContextPath() %>/js/owl.carousel.min.js"></script> 
<script type="text/javascript">
    //<![CDATA[
	jQuery(function() {
		jQuery(".slideshow").cycle({
			fx: 'scrollHorz', easing: 'easeInOutCubic', timeout: 10000, speedOut: 800, speedIn: 800, sync: 1, pause: 1, fit: 0, 			pager: '#home-slides-pager',
			prev: '#home-slides-prev',
			next: '#home-slides-next'
		});
	});
    //]]>
    </script>
</body>

<!-- Giao dien duoc chia se mien phi tai www.ptheme.net [Free HTML Download]. SKYPE[ptheme.net] - EMAIL[ptheme.net@gmail.com].-->
</html>