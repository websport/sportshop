<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>edit Account</title>
<!-- Favicons Icon -->
<link rel="icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />
<link rel="shortcut icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />

<!-- Mobile Specific -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/bootstrap.min.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/slider.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/owl.carousel.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/owl.theme.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/font-awesome.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/style.css" type="text/css">

<!-- Google Fonts -->
<style>
table {
    border-collapse: collapse;
    width: 450px;
    height: 320px;
    font-size: 15px;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color:#eacece}
</style>
</head>
<body>
<div class="page">
<%@include file="header1.jsp"%>
<%@include file="menu.jsp"%>
  <center><h3><b>Insert Product</b></h3>
         <h4><font color="red">${requestScope["message"]}</font></h4>
 <div class="container">
			<div class="card card-container"> 
	<center>
		<form action="<%=request.getContextPath() %>/UploadFile" method="post" enctype="multipart/form-data">
			<table border="1" cellpadding="5" cellspacing="1">
				<tr>
					<td>Product Name</td>
					<td><input type="text"placeholder="Enter product name" name="product_name" /></td>
				</tr>
				<tr>
					<td>Quantity</td>
					<td><input type="text" placeholder="Enter number" name="quantity"></td>
				</tr>
				<tr>
					<td>Date Update</td>
					<td><input type="date" name="date"></td>
				</tr>
				<tr>
					<td>Size</td>
					<td><select name="size_id"><b>
						<option value="1">S</option>
						<option value="2">M</option>
						<option value="3">L</option>
						<option value="4">XL</option>
						<option value="5">XXL</option>
						<option value="6">XXXL</option>
						<option value="7">1</option>
						<option value="8">2</option>
						<option value="9">3</option>
						<option value="10">4</option>
						<option value="11">5</option>
						<option value="12">32</option>
						<option value="13">33</option>
						<option value="14">34</option>
						<option value="15">35</option>
						<option value="16">36</option>
						<option value="17">37</option>
						<option value="18">38</option>
						<option value="19">39</option>
						<option value="20">40</option>
						<option value="21">41</option>
						<option value="22">42</option>
						<option value="23">43</option>
						<option value="24">44</option>
						<option value="25">45</option>
					</b></select></td>
				</tr>
				<tr>
					<td>Price</td>
					<td><input type="text"placeholder="Enter number" name="price"></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><input type="text" placeholder="Product details" name="description"></td>
				</tr>
				<tr>
					<td>Image</td>
					<td><input type="file" name="file"></td>
				</tr>
				<tr>
					<td>Status</td>
					<td><select name="status">
					    <option value="0">Club Clothes</option>
						<option value="1">National Clothes</option>
						<option value="2">Shoes and Ball</option>
						</select></td>
				</tr>
			</table>
			<br> <!--  <input type="submit" style="background-color: #3a96f2" name="action" value="Insert" />-->
			     <button type="submit" style="background-color: #7c7cd3" name="action" value="Insert" class="button btn-cart"><b>INSERT PRODUCT</b></button>
			     <button type="submit" style="background-color: #7c7cd3"value="Back" onclick="javascript:history.go(-1)" class="button btn-cart"><b>BACK</b></button>
		</form>
	</center>
</div>
</div>
<%@include file="footer.jsp"%>
</div>
</body>
</html>
