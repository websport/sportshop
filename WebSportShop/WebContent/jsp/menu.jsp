 <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
  <nav>
    <div class="container">
      <div class="nav-inner"> 
        <!-- mobile-menu -->
        <div class="hidden-desktop" id="mobile-menu">
          <ul class="navmenu">
            <li>
              <div class="menutop">
                <div class="toggle"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></div>
              </div>
		</ul>
        </div>
        
        <!--End mobile-menu -->
        <ul id="nav" class="hidden-xs">
          <li id="nav-home" class="level0 parent drop-menu active"><a href="<%=request.getContextPath()%>/"><span>Home</span> </a> </li>
          <li class="level0 nav-6 level-top parent"> <a href="information.jsp" class="level-top"> <span>Information</span> </a> </li>
         
          <li class="level0 parent drop-menu"><a href="#"><span>Clothes</span> </a>
            <ul style="display: none;" class="level1">
           
                  <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Arsenal"><span>Arsenal</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Manchester United"><span>Manchester United</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Barcelona"><span>Barcelona</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Bayern Munich"><span>Bayern Munich</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Manchester City"><span>Manchester City</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Real Madrid"><span>Real Madrid</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Chelsea"><span>Chelsea</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Liverpool"><span>Liverpool</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Juventus"><span>Juventus</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Paris Saint Germain"><span>Paris Saint-Germain</span></a></li>
          
            </ul>
          </li>
           <li class="level0 parent drop-menu"><a href="#"><span>Shoes</span> </a>
            <ul style="display: none;" class="level1">
                  <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Giay_Adidas"><span>Adidas</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Giay_Nike"><span>Nike</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Giay_Warriol"><span>Warriol</span></a></li>
            </ul>
          </li>
          <li class="level0 parent drop-menu"><a href="#"><span>Ball</span> </a>
            <ul style="display: none;" class="level1">
                  
                  <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Bong_Nike"><span>Nike</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Bong_New_Balance"><span>Balance</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Bong_Adidas"><span>Adidas</span></a></li>
                   <li class="level1"><a href="<%=request.getContextPath()%>/productController?formAction=showProductArsenal&name=Bong_Dynamics"><span>Dynamics</span></a></li>
            </ul>
          </li>
        </ul>
        <!-- Search-col -->
        <div class="search-box">
          <form id="search_mini_form" name="Categories" action="<%=request.getContextPath() %>/productController" method="post">
            <input type="text" placeholder="Search product here.." maxlength="70" name="searchproduct" id="search" >
            <button  class="btn btn-default  search-btn-bg" name="formAction" value="search" type="submit"> <span class="glyphicon glyphicon-search" ></span>&nbsp;</button>
          </form>
        </div>
        <!-- End Search-col --> 
      </div>
    </div>
  </nav>