

<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="en">

<head>
<meta charset="utf-8">
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Accord, premium HTML5 &amp; CSS3 template</title>

<!-- Favicons Icon -->
<link rel="icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />
<link rel="shortcut icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />

<!-- Mobile Specific -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">

<link type="text/css" rel="stylesheet"
	href="<%=request.getContextPath()%>/css/login.css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/bootstrap.min.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/slider.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/owl.carousel.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/owl.theme.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/font-awesome.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/style.css" type="text/css">

<!-- Google Fonts -->

</head>

<body class="cms-index-index">
	<div class="page">
		<!-- Header -->
		<header class="header-container">
			<div class="header-top">
				<div class="container">
					<div class="row">
						<div class="col-lg-5 col-md-5 col-xs-6">
							<div class="welcome-msg hidden-xs">
								<h2>Welcome to SD Shop</h2>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="header container">
				<div class="row">
					<div class="col-lg-3 col-sm-4 col-md-3">
						<!-- Header Logo -->
						<div class="logo">
							<a title="Magento Commerce" href=""><img
								alt="Magento Commerce"
								src="<%=request.getContextPath()%>/images/image_SDshop.png"></a>
						</div>
						<!-- End Header Logo -->
					</div>
					<div class="col-lg-9 col-xs-12">
						<div class="top-cart-contain">
							<div class="mini-cart">
								<div data-toggle="dropdown" data-hover="dropdown"
									class="basket dropdown-toggle">
									<a href="#"><span>Shopping Cart</span></a>
								</div>

							</div>

						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- end header -->
		<!-- Navbar -->
		<%@include file="menu.jsp"%>

		<div class="container">
			<div class="card card-container">
				<%@include file="message.jsp"%>
				<form class="form-signin"
					action="<%=request.getContextPath()%>/userController"
					method="POST">
					<span id="reauth-email" class="reauth-email"></span> <label><b>Full
							Name:</b> </label> <input type="text" name="fullName" /> <label><b>Address:</b></label>
					<input type="text" name="address" /> <label><b>Phone:</b>
					</label> <input type="text" name="phone" /> </br> <label><b>Gender:</b>
					</label> <select name="gender">
						<option value="1"><b>MALE</b></option>
						<option value="0">FEMALE</option>
					</select> </br>
					</br> <label><b>Identity Card:</b> </label> <input type="text"
						name="identityCard" /> <label><b>User Name:</b> </label> <input
						type="text" name="userName" /> <label><b>Password:</b> </label> <input
						type="password" name="password" /> </br> <label><b>Role:</b> </label>
					<select name="role">
						<option value="2">BOSS</option>
						<option value="3">STAFF</option>
					</select></br>
					</br>
					</br>
					<button class="btn btn-lg btn-primary btn-block btn-signin"
						type="submit" name="action" value="register">Register</button>
				</form>
					<button class="btn btn-lg btn-primary btn-block btn-signin"
						value="Back" onclick="javascript:history.go(-1)">Cancel</button>
				</br>
				<!-- /form -->
			</div>
			<!-- /card-container -->
		</div>
		<!-- Footer -->
		<%@include file="footer.jsp"%>
		<!-- End Footer -->

	</div>


	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/parallax.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/common.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/slider.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/owl.carousel.min.js"></script>
	<script type="text/javascript">
		//<![CDATA[
		jQuery(function() {
			jQuery(".slideshow").cycle({
				fx : 'scrollHorz',
				easing : 'easeInOutCubic',
				timeout : 10000,
				speedOut : 800,
				speedIn : 800,
				sync : 1,
				pause : 1,
				fit : 0,
				pager : '#home-slides-pager',
				prev : '#home-slides-prev',
				next : '#home-slides-next'
			});
		});
		//]]>
	</script>
</body>