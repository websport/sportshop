<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.pnv.model.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8 ">
<title>Delete Product</title>
<!-- Favicons Icon -->
<link rel="icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />
<link rel="shortcut icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />

<!-- Mobile Specific -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/bootstrap.min.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/slider.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/owl.carousel.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/owl.theme.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/font-awesome.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/style.css" type="text/css">

<!-- Google Fonts -->

<style>
table {
    border-collapse: collapse;
    width: 70%;
    font-size: 15px;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
</head>
<body class="cms-index-index">
<div class="page">
<%@include file="header1.jsp"%>
<%@include file="menu.jsp"%>
	<center>
		<h1><b>Show Product</b></h1>
		 <p style="color: red;">${MS}</p>
	</center>
   
	<p style="color: red;">${errorString}</p>
	<center>
		<table border="5" cellpadding="5" cellspacing="1">
			<th>productId</th>
			<th>productName</th>
			<th>quantity</th>
			<th>dateUpdate</th>
			<th>sizeId</th>
			<th>price</th>
			<th>description</th>
			<th>Delete</th>
			<th>EDIT</th>
			</tr>
			<c:forEach items="${delete_pro}" var="delete_pro">
				<tr>
					<td>${delete_pro.productId}</td>
					<td>${delete_pro.productName}</td>
					<td>${delete_pro.quantity}</td>
					<td>${delete_pro.dateUpdate}</td>
					<td>${delete_pro.sizeId}</td>
					<td>${delete_pro.price}</td>
					<td>${delete_pro.description}</td>
					<td>
						<!--  <form class="form-signin" action="show" method="POST">
							<button type="submit" class="cil" name="action"
								value="deleteForm">Delete</button>
						</form>--> <!--   <a
						 href="<%=request.getContextPath()%>/show?formAction=deleteProductByID&id=${delete_pro.productId}">
						<button style="background-color: white" type="submit" class="cil"name="formAction" value="deleteProductByID">Delete</button>
						</a> 
						-->

						<form class="form-signin"
							action="<%=request.getContextPath()%>/productController" method="GET">
							<input type="hidden" name="submit_id"
								value="${delete_pro.productId}" />
							<button type="submit" class="button btn-cart" name="formAction"
								value="deleteProductByID" onclick="return confirm('Are you sure?')"><b>DELETE</b></button>
					</td>
					<td>	
					</form>
							<form class="form-signin"
							action="<%=request.getContextPath()%>/productController" method="GET">
							<input type="hidden" name="submit_id"
								value="${delete_pro.productId}" />
							<button type="submit" class="button btn-cart" name="formAction"
								value="editProduct"><b>EDIT PRODUCT</b></button>
						</form>
					 </td>
				</tr>
			</c:forEach>
		</table>
		<center>
			<tr>
				<td><b><input style="background-color: " type="button" 
					value="BACK" onclick="javascript:history.go(-1)"></b></td>
			</tr>
		</center>
	</center>
	<%@include file="footer.jsp"%>
</body>
</html>