<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>User Account</title>
<!-- Favicons Icon -->
<link rel="icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />
<link rel="shortcut icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />

<!-- Mobile Specific -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/bootstrap.min.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/slider.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/owl.carousel.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/owl.theme.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/font-awesome.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/style.css" type="text/css">

<!-- Google Fonts -->

<style>
table {
    border-collapse: collapse;
    width: 105%;
    
    font-size: 15px;
}

th, td {
    text-align: center;
    padding: 8px;
}

tr:nth-child(even){background-color: #daccea}
</style>
</head>
<body class="cms-index-index">
<div class="page">
<%@include file="header1.jsp"%>
<%@include file="menu.jsp"%>
  <center><h1>User Account</h1></center>
 <div class="container">
			<div class="card card-container"> 
	<center>
	<%@include file="message.jsp"%>
		<table border="5" cellpadding="5" cellspacing="1">
			<tr>
				<th><b>ID</b></th>
				<th><b>NAME</b></th>
				<th><b>ADDRESS</b></th>
				<th><b>PHONE</b></th>
				<th><b>GENDER</b></th>
				<th><b>IDENTITYCARD</b></th>
				<th><b>USERNAME</b></th>
				<th><b>PASSWORD</b></th>
				<th><b>EDIT</b></th>
				<th><b>DELETE</b></th>
			</tr>
			<c:forEach items="${user}" var="user">
				<tr>
					<td><b><c:out value="${user.id}" /></b></td>
					<td><b>${user.name}</b></td>
					<td><b>${user.address}</b></td>
					<td><b>${user.phone}</b></td>
					<td><b><c:choose>
							<c:when test="${user.gender == 'true'}"> Male  <br />
							</c:when>
							<c:otherwise> Female <br />
							</c:otherwise>
						</c:choose></b></td>
					<td><b>${user.identityCard}</b></td>
					<td><b>${user.userName}</b></td>
					<td><b>${user.password}</b></td>
					<td>
					  <a
							href="<%=request.getContextPath()%>/userController?formAction=editForm&idEdit=${user.id}">
							<button style="background-color: #d1833a" class="button btn-cart" type="submit"><b>EDIT</b></button>
							
						</a>
					</td>
					<td>
						<a
							href="<%=request.getContextPath()%>/userController?formAction=deleteSuccess&id=${user.id}" onclick="return confirm('Are you sure?')">
						    <button style="background-color: #3a96f2" class="button btn-cart" type="submit"><b>Delete</b></button> 			
						</a>
					</td>
				</tr>
			</c:forEach>
		</table>
		<center><tr><td><h3><B><input style="background-color: " class="button btn-cart" type="button" value="Back" onclick="javascript:history.go(-1)"></B></h3></td></tr></center>
	</center>
	</div>
	</div>
<%@include file="footer.jsp"%>
</div>
</body>
</html>