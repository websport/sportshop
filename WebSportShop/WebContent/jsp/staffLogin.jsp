<!DOCTYPE html>
<%@page import="com.pnv.model.StaffInformation"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="en">

<head>
<meta charset="utf-8">
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Accord, premium HTML5 &amp; CSS3 template</title>

<!-- Favicons Icon -->
<link rel="icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />
<link rel="shortcut icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />

<!-- Mobile Specific -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">

<link type="text/css" rel="stylesheet"
	href="<%=request.getContextPath()%>/css/login.css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/bootstrap.min.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/slider.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/owl.carousel.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/owl.theme.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/font-awesome.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/style.css" type="text/css">

<!-- Google Fonts -->

</head>

<body class="cms-index-index">
	<div class="page">
		<!-- Header -->
		<header class="header-container">
			<div class="header-top">
				<div class="container">
					<div class="row">
						<div class="col-lg-5 col-md-5 col-xs-6">

							<div class="welcome-msg hidden-xs">
								<h2>Welcome to SD Shop</h2>
							</div>
						</div>
						<div class="col-lg-7 col-md-7 col-xs-6">

							<!-- Header Top Links -->
							<div class="toplinks">
								<div class="links">

									</br>
									<%@include file="message.jsp"%>
									<form class="form-signin" action="userController" method="GET">
										<button type="submit" class="button btn-cart"
											name="formAction" value="changePassword">
											<b>CHANGE PASSWORD</b>
										</button>
										</a>
									</form>
									<form class="form-signin"
										action="<%=request.getContextPath()%>/productController"
										method="POST">
										<button type="submit" class="button ctn-cart" value="order"
											name="formAction">
											<b>ORDER</b>
										</button>
									</form>
									<form class="form-signin" action="userController" method="POST">
										<button type="submit" class="button btn-cart" value="logout"
											name="action">
											<b>LOGOUT</b>
										</button>
										</a>
									</form>
								</div>
							</div>
							<!-- End Header Top Links -->
						</div>
					</div>
				</div>
			</div>

			<div class="header container">
				<div class="row">
					<div class="col-lg-3 col-sm-4 col-md-3">
						<!-- Header Logo -->
						<div class="logo">
							<a title="Magento Commerce" href=""><img
								alt="Magento Commerce"
								src="<%=request.getContextPath()%>/images/image_SDshop.png"></a>
						</div>
						<!-- End Header Logo -->
					</div>

				</div>
			</div>
		</header>
		<!-- end header -->
		<!-- Navbar -->
		<%@include file="menu.jsp"%>
		<%@include file="slide.jsp"%>
		<%@include file="container.jsp"%>



		<!-- Footer -->
		<%@include file="footer.jsp"%>
		<!-- End Footer -->

	</div>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/parallax.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/common.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/slider.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/js/owl.carousel.min.js"></script>
	<script type="text/javascript">
		//<![CDATA[
		jQuery(function() {
			jQuery(".slideshow").cycle({
				fx : 'scrollHorz',
				easing : 'easeInOutCubic',
				timeout : 10000,
				speedOut : 800,
				speedIn : 800,
				sync : 1,
				pause : 1,
				fit : 0,
				pager : '#home-slides-pager',
				prev : '#home-slides-prev',
				next : '#home-slides-next'
			});
		});
		//]]>
	</script>
</body>

<!-- Giao dien duoc chia se mien phi tai www.ptheme.net [Free HTML Download]. SKYPE[ptheme.net] - EMAIL[ptheme.net@gmail.com].-->
</html>