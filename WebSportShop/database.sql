-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: database
-- ------------------------------------------------------
-- Server version	5.6.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_order` date NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `total` decimal(10,3) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `customer_name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `fk_order_staff_information1_idx` (`staff_id`),
  CONSTRAINT `fk_order_staff_information1` FOREIGN KEY (`staff_id`) REFERENCES `staff_information` (`staff_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `date_update` date NOT NULL,
  `size_id` int(11) NOT NULL,
  `price` decimal(7,3) NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  KEY `fk_product_size1_idx` (`size_id`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `description_2` (`description`),
  CONSTRAINT `fk_product_size1` FOREIGN KEY (`size_id`) REFERENCES `size` (`size_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Arsenal',20,'2016-11-14',1,200.000,'null','Product_Arsenal1.jpg',0),(2,'Arsenal',20,'2016-11-14',2,250.000,'Arsenal','Product_Arsenal2.jpg',0),(3,'Arsenal',20,'2016-11-14',3,300.000,'Arsenal','Product_Arsenal3.jpg',0),(4,'Arsenal',20,'2016-11-14',4,300.000,'Arsenal','Product_Arsenal4.jpg',0),(5,'Arsenal',20,'2016-11-14',5,200.000,'NULL','Product_Arsenal5.jpg',0),(6,'Arsenal',20,'2016-11-14',6,200.000,'NULL','Product_Arsenal6.jpg',0),(7,'Manchester United',20,'2016-11-14',1,250.000,'Mu Manchester_United Manchester United','Product_Manchester United7.jpg',0),(8,'Manchester United',20,'2016-11-14',2,150.000,'Mu Manchester_United Manchester United','Product_Manchester United8.jpg',0),(9,'Manchester United',20,'2016-11-14',3,150.000,'  ','Product_Manchester United9.jpg',0),(10,'Manchester United',20,'2016-11-14',4,150.000,'','Product_Manchester United10.jpg',0),(11,'Manchester United',20,'2016-11-14',5,150.000,'NULL','Product_Manchester United11.jpg',0),(12,'Manchester United',20,'2016-11-14',6,150.000,'NULL','Product_Manchester United12.jpg',0),(13,'Barcelona',20,'2016-11-14',1,125.000,'Barcelona barca','Product_Barcelona13.jpg',0),(14,'Barcelona',20,'2016-11-14',2,215.000,'Barcelona barca','Product_Barcelona14.jpg',0),(15,'Barcelona',20,'2016-11-14',3,115.000,'Barcelona barca','Product_Barcelona15.jpg',0),(16,'Barcelona',20,'2016-11-14',4,115.000,'NULL','Product_Barcelona16.jpg',0),(17,'Barcelona',20,'2016-11-14',5,115.000,'NULL','Product_Barcelona17.jpg',0),(18,'Barcelona',20,'2016-11-14',6,115.000,'NULL','Product_Barcelona18.jpg',0),(19,'Bayern Munich',20,'2016-11-14',1,90.000,'Bayern Munich','Product_Bayern Munich19.jpg',0),(20,'Bayern Munich',20,'2016-11-14',2,90.000,'Bayern Munich','Product_Bayern Munich20.jpg',0),(21,'Bayern Munich',20,'2016-11-14',3,90.000,'NULL','Product_Bayern Munich21.jpg',0),(22,'Bayern Munich',20,'2016-11-14',4,90.000,'NULL','Product_Bayern Munich22.jpg',0),(23,'Bayern Munich',20,'2016-11-14',5,90.000,'NULL','Product_Bayern Munich23.jpg',0),(24,'Bayern Munich',20,'2016-11-14',6,90.000,'NULL','Product_Bayern Munich24.jpg',0),(25,'Manchester City',20,'2016-11-14',1,180.000,'Manchester City Manchester_City','Product_Manchester City25.jpg',0),(26,'Manchester City',20,'2016-11-14',2,180.000,'Manchester City Manchester_City','Product_Manchester City26.jpg',0),(27,'Manchester City',20,'2016-11-14',3,180.000,'Manchester City Manchester_City','Product_Manchester City27.jpg',0),(28,'Manchester City',20,'2016-11-14',4,180.000,'NULL','Product_Manchester City28.jpg',0),(29,'Manchester City',20,'2016-11-14',5,180.000,'NULL','Product_Manchester City29.jpg',0),(30,'Manchester City',20,'2016-11-14',6,180.000,'NULL','Product_Manchester City30.jpg',0),(31,'Real Madrid',20,'2016-11-14',1,120.000,'Real Madrid',NULL,0),(32,'Real Madrid',20,'2016-11-14',2,120.000,'Real Madrid',NULL,0),(33,'Real Madrid',20,'2016-11-14',3,120.000,'Real Madrid',NULL,0),(34,'Real Madrid',20,'2016-11-14',4,120.000,'NULL',NULL,0),(35,'Real Madrid',20,'2016-11-14',5,120.000,'NULL',NULL,0),(36,'Real Madrid',20,'2016-11-14',6,120.000,'NULL',NULL,0),(37,'Chelsea',20,'2016-11-14',1,270.000,'Chelsea','Product_Chelsea37.jpg',0),(38,'Chelsea',20,'2016-11-14',2,70.000,'Chelsea','Product_Chelsea38.jpg',0),(39,'Chelsea',20,'2016-11-14',3,70.000,'Chelsea','Product_Chelsea39.jpg',0),(40,'Chelsea',20,'2016-11-14',4,70.000,'NULL','Product_Chelsea40.jpg',0),(41,'Chelsea',20,'2016-11-14',5,70.000,'NULL','Product_Chelsea41.jpg',0),(42,'Chelsea',20,'2016-11-14',6,70.000,'NULL','Product_Chelsea42.jpg',0),(43,'Liverpool',20,'2016-11-14',1,95.000,'Liverpool','Product_Liverpool43.jpg',0),(44,'Liverpool',20,'2016-11-14',2,295.000,'Liverpool','Product_Liverpool44.jpg',0),(45,'Liverpool',20,'2016-11-14',3,95.000,'NULL','Product_Liverpool45.jpg',0),(46,'Liverpool',20,'2016-11-14',4,95.000,'NULL','Product_Liverpool46.jpg',0),(47,'Liverpool',20,'2016-11-14',5,95.000,'NULL','Product_Liverpool47.jpg',0),(48,'Liverpool',20,'2016-11-14',6,95.000,'NULL','Product_Liverpool48.jpg',0),(49,'Juventus',20,'2016-11-14',1,120.000,'NULL','Product_Juventus49.jpg',0),(50,'Juventus',20,'2016-11-14',2,120.000,'NULL','Product_Juventus50.jpg',0),(51,'Juventus',20,'2016-11-14',3,120.000,'Juventus','Product_Juventus51.jpg',0),(52,'Juventus',20,'2016-11-14',4,120.000,'Juventus','Product_Juventus52.jpg',0),(53,'Juventus',20,'2016-11-14',5,120.000,'NULL','Product_Juventus53.jpg',0),(54,'Juventus',20,'2016-11-14',6,120.000,'NULL','Product_Juventus54.jpg',0),(55,'Paris Saint Germain',20,'2016-11-14',1,160.000,'NULL','Product_Paris Saint Germain55.jpg',0),(56,'Paris Saint Germain',20,'2016-11-14',2,160.000,'NULL','Product_Paris Saint Germain56.jpg',0),(57,'Paris Saint Germain',20,'2016-11-14',3,160.000,'Paris Saint Germain PSG','Product_Paris Saint Germain57.jpg',0),(58,'Paris Saint Germain',20,'2016-11-14',4,160.000,'Paris Saint Germain PSG','Product_Paris Saint Germain58.jpg',0),(59,'Paris Saint Germain',20,'2016-11-14',5,160.000,'NULL','Product_Paris Saint Germain59.jpg',0),(60,'Paris Saint Germain',20,'2016-11-14',6,160.000,'NULL','Product_Paris Saint Germain60.jpg',0),(61,'Giay_Adidas',20,'2016-11-16',12,500.000,'','Product_Giay_Adidas61.jpg',2),(62,'Giay_Adidas',20,'2016-11-16',13,500.000,'Adidas ','Product_Giay_Adidas62.jpg',2),(63,'Giay_Adidas',20,'2016-11-16',14,500.000,'Adidas ','Product_Giay_Adidas63.jpg',2),(64,'Giay_Adidas',20,'2016-11-16',15,500.000,'Adidas ','Product_Giay_Adidas64.jpg',2),(65,'Giay_Adidas',20,'2016-11-16',16,500.000,'NULL','Product_Giay_Adidas65.jpg',2),(66,'Giay_Adidas',20,'2016-11-16',17,500.000,'NULL','Product_Giay_Adidas66.jpg',2),(67,'Giay_Adidas',20,'2016-11-16',18,500.000,'NULL','Product_Giay_Adidas67.jpg',2),(68,'Giay_Adidas',20,'2016-11-16',19,500.000,'NULL','Product_Giay_Adidas68.jpg',2),(69,'Giay_Adidas',20,'2016-11-16',20,500.000,'NULL','Product_Giay_Adidas69.jpg',2),(70,'Giay_Adidas',20,'2016-11-16',21,500.000,'NULL','Product_Giay_Adidas70.jpg',2),(71,'Giay_Adidas',20,'2016-11-16',22,500.000,'NULL','Product_Giay_Adidas71.jpg',2),(72,'Giay_Adidas',20,'2016-11-16',23,500.000,'NULL','Product_Giay_Adidas72.jpg',2),(73,'Giay_Adidas',20,'2016-11-16',24,500.000,'NULL','Product_Giay_Adidas73.jpg',2),(74,'Giay_Adidas',20,'2016-11-16',25,500.000,'NULL','Product_Giay_Adidas74.jpg',2),(75,'Giay_Nike',20,'2016-11-16',12,450.000,'NULL','Product_Giay_Nike75.jpg',2),(76,'Giay_Nike',20,'2016-11-16',13,450.000,'NULL','Product_Giay_Nike76.jpg',2),(77,'Giay_Nike',20,'2016-11-16',14,450.000,'Nike','Product_Giay_Nike77.jpg',2),(78,'Giay_Nike',20,'2016-11-16',15,450.000,'Nike','Product_Giay_Nike78.jpg',2),(79,'Giay_Nike',20,'2016-11-16',16,450.000,'Nike','Product_Giay_Nike79.jpg',2),(80,'Giay_Nike',20,'2016-11-16',17,450.000,'NULL','Product_Giay_Nike80.jpg',2),(81,'Giay_Nike',20,'2016-11-16',18,450.000,'NULL','Product_Giay_Nike81.jpg',2),(82,'Giay_Nike',20,'2016-11-16',19,450.000,'NULL','Product_Giay_Nike82.jpg',2),(83,'Giay_Nike',20,'2016-11-16',20,450.000,'NULL','Product_Giay_Nike83.jpg',2),(84,'Giay_Nike',20,'2016-11-16',21,450.000,'NULL','Product_Giay_Nike84.jpg',2),(85,'Giay_Nike',20,'2016-11-16',22,450.000,'NULL','Product_Giay_Nike85.jpg',2),(86,'Giay_Nike',20,'2016-11-16',23,450.000,'NULL','Product_Giay_Nike86.jpg',2),(87,'Giay_Nike',20,'2016-11-16',24,450.000,'NULL','Product_Giay_Nike87.jpg',2),(88,'Giay_Nike',20,'2016-11-16',25,450.000,'NULL','Product_Giay_Nike88.jpg',2),(89,'Giay_Warriol',20,'2016-11-16',12,400.000,'NULL','Product_Giay_Warriol89.jpg',2),(90,'Giay_Warriol',20,'2016-11-16',13,400.000,'Warriol','Product_Giay_Warriol90.jpg',2),(91,'Giay_Warriol',20,'2016-11-16',14,400.000,'Warriol','Product_Giay_Warriol91.jpg',2),(92,'Giay_Warriol',20,'2016-11-16',15,400.000,'Warriol','Product_Giay_Warriol92.jpg',2),(93,'Giay_Warriol',20,'2016-11-16',16,400.000,'NULL','Product_Giay_Warriol93.jpg',2),(94,'Giay_Warriol',20,'2016-11-16',17,400.000,'NULL','Product_Giay_Warriol94.jpg',2),(95,'Giay_Warriol',20,'2016-11-16',18,400.000,'NULL','Product_Giay_Warriol95.jpg',2),(96,'Giay_Warriol',20,'2016-11-16',19,400.000,'NULL','Product_Giay_Warriol96.jpg',2),(97,'Giay_Warriol',20,'2016-11-16',20,400.000,'NULL','Product_Giay_Warriol97.jpg',2),(98,'Giay_Warriol',20,'2016-11-16',21,400.000,'NULL','Product_Giay_Warriol98.jpg',2),(99,'Giay_Warriol',20,'2016-11-16',22,400.000,'NULL','Product_Giay_Warriol99.jpg',2),(100,'Giay_Warriol',20,'2016-11-16',23,400.000,'NULL','Product_Giay_Warriol100.jpg',2),(101,'Giay_Warriol',20,'2016-11-16',24,400.000,'NULL','Product_Giay_Warriol101.jpg',2),(102,'Giay_Warriol',20,'2016-11-16',25,400.000,'NULL','Product_Giay_Warriol102.jpg',2),(103,'Bong_Dynamics',20,'2016-11-16',7,250.000,'Dynamics Dong Luc','Product_Bong_Dynamics103.jpg',2),(104,'Bong_Dynamics',20,'2016-11-16',8,250.000,'Dynamics Dong Luc','Product_Bong_Dynamics104.jpg',2),(105,'Bong_Dynamics',20,'2016-11-16',9,250.000,'Dynamics Dong Luc','Product_Bong_Dynamics105.jpg',2),(106,'Bong_Dynamics',20,'2016-11-16',10,250.000,'Dynamics Dong Luc','Product_Bong_Dynamics106.jpg',2),(107,'Bong_Dynamics',20,'2016-11-16',11,250.000,'NULL','Product_Bong_Dynamics107.jpg',2),(108,'Bong_Dynamics',20,'2016-11-16',7,539.000,'NULL','Product_Bong_Dynamics108.jpg',2),(109,'Bong_New_Balance',20,'2016-11-16',8,539.000,'New Balance','Product_Bong_New_Balance109.jpg',2),(110,'Bong_New_Balance',20,'2016-11-16',9,539.000,'New Balance','Product_Bong_New_Balance110.jpg',2),(111,'Bong_New_Balance',20,'2016-11-16',10,539.000,'New Balance','Product_Bong_New_Balance111.jpg',2),(112,'Bong_New_Balance',20,'2016-11-16',11,539.000,'New Balance','Product_Bong_New_Balance111.jpg',2),(113,'Bong_Nike',20,'2016-11-16',7,149.000,'NULL','Product_Bong_Nike113.jpg',2),(114,'Bong_Nike',20,'2016-11-16',8,149.000,'NULL','Product_Bong_Nike114.jpg',2),(115,'Bong_Nike',20,'2016-11-16',9,149.000,'Nike','Product_Bong_Nike115.jpg',2),(116,'Bong_Nike',20,'2016-11-16',10,149.000,'Nike','Product_Bong_Nike116.jpg',2),(117,'Bong_Nike',20,'2016-11-16',11,149.000,'Nike','Product_Bong_Nike117.jpg',2),(118,'Bong_Adidas',20,'2016-11-16',7,200.000,'Adidas','Product_Bong_Adidas118.jpg',2),(119,'Bong_Adidas',20,'2016-11-16',8,200.000,'Adidas','Product_Bong_Adidas119.jpg',2),(120,'Bong_Adidas',20,'2016-11-16',9,200.000,'Adidas','Product_Bong_Adidas120.jpg',2),(121,'Bong_Adidas',20,'2016-11-16',10,200.000,'NULL','Product_Bong_Adidas121.jpg',2),(122,'Bong_Adidas',20,'2016-11-16',11,200.000,'NULL','Product_Bong_Adidas122.jpg',2),(123,'Real Madrid',25,'2016-12-08',1,100.000,'Real Madrid','Product_Real Madrid123.jpg',0);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_order`
--

DROP TABLE IF EXISTS `product_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_order` (
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`order_id`,`product_id`),
  KEY `fk_product_order_product1_idx` (`product_id`),
  CONSTRAINT `fk_product_order_order1` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_order_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_order`
--

LOCK TABLES `product_order` WRITE;
/*!40000 ALTER TABLE `product_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'System'),(2,'Boss'),(3,'Staff');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `size`
--

DROP TABLE IF EXISTS `size`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `size` (
  `size_id` int(11) NOT NULL,
  `type_of_size` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`size_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `size`
--

LOCK TABLES `size` WRITE;
/*!40000 ALTER TABLE `size` DISABLE KEYS */;
INSERT INTO `size` VALUES (1,'S'),(2,'M'),(3,'L'),(4,'XL'),(5,'XXL'),(6,'XXXL'),(7,'1'),(8,'2'),(9,'3'),(10,'4'),(11,'5'),(12,'32'),(13,'33'),(14,'34'),(15,'35'),(16,'36'),(17,'37'),(18,'38'),(19,'39'),(20,'40'),(21,'41'),(22,'42'),(23,'43'),(24,'44'),(25,'45');
/*!40000 ALTER TABLE `size` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_information`
--

DROP TABLE IF EXISTS `staff_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_information` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(11) CHARACTER SET utf8mb4 DEFAULT NULL,
  `gender` bit(1) DEFAULT NULL,
  `identity_card` varchar(9) CHARACTER SET utf8mb4 DEFAULT NULL,
  `username` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `description_password` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`staff_id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `identity_card_UNIQUE` (`identity_card`),
  UNIQUE KEY `phone_UNIQUE` (`phone`),
  KEY `fk_staff_information_role1_idx` (`role_id`),
  CONSTRAINT `fk_staff_information_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_information`
--

LOCK TABLES `staff_information` WRITE;
/*!40000 ALTER TABLE `staff_information` DISABLE KEYS */;
INSERT INTO `staff_information` VALUES (2,'Phan Thiện Quang','40 Lê Duẩn','01668002368','','201935651','quangpt@gmail.com','4477c3f541110b1ac437af1d8710e03e',2,'quangpt'),(3,'Nguyễn Thị Mai','08 Phan Châu Trinh','0905723070','\0','201731818','maint@gmail.com','67f50785d40416a7a7689c5c353f1128',3,'maint'),(4,'Nguyễn Ngọc Đa','02 Thanh Sơn','01264471642','','201722252','dann@gmail.com','202cb962ac59075b964b07152d234b70',3,'123'),(5,'Đinh Thanh Hà','36 Nguyễn Tất Thành','0122352164','\0','201764342','hadt@gmail.com','e8711ed884820c1ae82efad517b1c263',3,'hadt'),(8,'Lý Thị Trung Nhẫn','02 Thanh Sơn','0906500617','\0','205646548','nhanltt@gmail.com','3213c8f34e2e155296861c9c2b773862',3,'nhanltt'),(22,'Nguyễn Hữu Tường Vi','12 Đinh Tiên Hoàng','0908123123','','205646542','vinht@gmail.com','b83e97344e826b1463a94eb5e5902daf',3,'vinht');
/*!40000 ALTER TABLE `staff_information` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-09 10:12:39
