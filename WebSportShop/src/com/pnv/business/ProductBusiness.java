package com.pnv.business;

import java.util.ArrayList;
import java.util.List;

import com.pnv.model.Order;
import com.pnv.model.Product;


public interface ProductBusiness {
	ArrayList<Product> showProduct();
	boolean deleteProductBo(int productId);

	List<Product> showProductClub();
    List<Product> showProductNational();
    List<Product> showProductSB();
    List<Product> showProductClubArsenal(String nameClub);
    Product findProduct(int productId);
	ArrayList<Product>searchProduct(String values);
//	boolean orderProduct(Order order);
	List<Order> showOrder();
	boolean sellProduct(int staffId, int orderId, boolean status);
	boolean deleteOrder(int orderId);
	boolean createProduct(String productName, int quantity, String dateUpdate, String sizeId, float price,
			String description, String image, int status);
	boolean updateProduct(int productId, String namePr, int quantity, String date, String size, float price,
			String description, String image, int status);
}
