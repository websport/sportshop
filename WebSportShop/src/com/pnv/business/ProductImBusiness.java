package com.pnv.business;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.pnv.dao.ProductImDao;
import com.pnv.model.Order;
import com.pnv.model.Product;
import com.pnv.utils.DbHelper;

public class ProductImBusiness implements ProductBusiness {
	
	@Override
	public boolean deleteProductBo(int productId) {
		// TODO Auto-generated method stub
		
		try {
			Connection connection = DbHelper.returnDbHelper();
			
			ProductImDao deleteProduct = new ProductImDao();
			boolean result = deleteProduct.deleteProduct(productId);
			if (result) {
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public ArrayList<Product> showProduct() {
		// TODO Auto-generated method stub
		ArrayList<Product> product = new ArrayList<>();
		ProductImDao showPro = new ProductImDao();
		product = showPro.showProductDAO();
		return product;

	}

	@Override
	public List<Product> showProductClub() {
		List<Product> product = new ArrayList<>();
		ProductImDao showPro = new ProductImDao();
		product = showPro.showProductClub();
		// TODO Auto-generated method stub
		return product;
	}

	@Override

	public List<Product> showProductNational() {
		List<Product> product = new ArrayList<>();
		ProductImDao showPro = new ProductImDao();
		product = showPro.showProductNational();
		// TODO Auto-generated method stub
		return product;
	}

	@Override
	public List<Product> showProductSB() {
		List<Product> product = new ArrayList<>();
		ProductImDao showPro = new ProductImDao();
		product = showPro.showProductSB();
		// TODO Auto-generated method stub
		return product;
	}

	@Override
	public Product findProduct(int productId) {
		ProductImDao showProduct=new ProductImDao();
		Product show =showProduct.showProduct(productId);
		if(show!=null){
			return show;
		}
		// TODO Auto-generated method stub
		return null;
	}


	

	public ArrayList<Product> searchProduct(String values) {
		 ArrayList<Product> list= new ArrayList<>();
		 ProductImDao pid= new ProductImDao();
		 list =pid.searching(values);		
			return list;
	}

	@Override
	public List<Product> showProductClubArsenal(String nameClub) {
		List<Product> product = new ArrayList<>();
		ProductImDao showPro = new ProductImDao();
		product = showPro.showProductClubArsenal(nameClub);
		// TODO Auto-generated method stub
		return product;
	}


	@Override
	public boolean createProduct(String productName, int quantity, String dateUpdate, String sizeId, float price,
			String description, String image, int status) {
		Product product=new Product(productName,quantity,dateUpdate,sizeId,price,description,image,status);
		ProductImDao create=new ProductImDao();
		boolean  result= create.createProduct(product);
		if(result){
			return true;
		}
		// TODO Auto-generated method stub
		return false;
	}


//	@Override
//	public boolean orderProduct(Order order) {
//		ProductImDao pro = new ProductImDao();
//		boolean result = pro.insertOrderProduct(order);
//		if(result){
//			return true;
//		}
//		return false;
//	}


	@Override
	public List<Order> showOrder() {
		ProductImDao show = new ProductImDao();
		List<Order> list = show.showOrder();
		
		for(int i = 0; i < list.size();i ++){
			list.get(i).setProducts(show.showProductOrder(list.get(i).getOrderId()));
		}
		return list;
	}

	@Override
	public boolean sellProduct(int staffId, int orderId, boolean status) {
		ProductImDao pro = new ProductImDao();
		boolean result = pro.sellProduct(staffId, orderId, status);
		if(result == true){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteOrder(int orderId) {
		ProductImDao pro = new ProductImDao();
		boolean result = pro.deleteOrder(orderId);
		if(result){
			return true;
		}
		return false;
	}

	@Override
	public boolean updateProduct( int productId, String namePr, int quantity, String date, String size, float price,
			String description, String image, int status) {
		Product a=new Product();
		ProductImDao update=new ProductImDao();
		boolean result=update.editProduct(productId,namePr,quantity,date,size,price,description,image,status);
		// TODO Auto-generated method stub
		if(result){
			return true;
		}
		return false;
	}

	

	
}
