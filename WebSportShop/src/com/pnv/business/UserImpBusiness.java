package com.pnv.business;

import java.util.ArrayList;
import java.util.List;

import com.pnv.dao.UserImpDao;
import com.pnv.model.StaffInformation;
import com.pnv.utils.Utils;

public class UserImpBusiness implements UserBusiness{

	@Override
	public boolean createNewStaff(String name, String address, String phone, boolean gender, int identityCard,
			String userName, String password, int role) {
		
		StaffInformation staff = new StaffInformation(name,address,phone,gender,identityCard,userName,password,role);
		
		UserImpDao newStaff = new UserImpDao();
		
		boolean result = newStaff.createNewStaff(staff);
		
		if(result){
			return true;
		}
		return false;
	}

	@Override
	public int checkLogin(String userName, String password) {
		
		UserImpDao checkStaff = new UserImpDao();
			
		String pwmd5 = Utils.getMD5(password);
		
		int userNameCorrect = checkStaff.selectStaffId(userName, pwmd5);

		if(userNameCorrect != 0){
			return userNameCorrect;
		}
		return 0;	
}

	@Override
	public boolean changePassword(String userName, String password, String newPassword) {
		UserImpDao changePass = new UserImpDao();
		boolean result = changePass.updatePassword(userName, password, newPassword);
		if(result){
			return true;
		}
		return false;
	}

	@Override
	public List<StaffInformation> showStaff() {
		List<StaffInformation> staff = new ArrayList<>();
		UserImpDao showStaff = new UserImpDao();
		staff = showStaff.queryStaff();
		return staff;
	}

	@Override
	public boolean deleteStaffById(int id) {
		UserImpDao delete=new UserImpDao();
		boolean deleteAccount=delete.deleteStaffById(id);
		if(deleteAccount){
			return true;
		}
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public StaffInformation findUser(int staffId) {
		UserImpDao showEdit=new UserImpDao();
		StaffInformation show = showEdit.showStaff(staffId);
		if(show!=null){
			return show;
		}
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean changeInformationOfStaff(int id,String address, String phone,String userName, String password) {
		UserImpDao change = new UserImpDao();
		boolean result = change.updateInformationOfStaff(id,address,phone,userName,password);
		if(result){
			return true;
		}
		return false;
	}
	
	public static void main (String [] args){
		UserImpBusiness user = new UserImpBusiness();
		boolean result = user.changeInformationOfStaff(7,"12 Nguyễn Trãi","0548134125", "antnt@gmail.com", "an123");
		System.out.println(result);
	}
	
}
