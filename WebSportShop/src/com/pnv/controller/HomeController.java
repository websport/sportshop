package com.pnv.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pnv.business.ProductImBusiness;
import com.pnv.model.Product;



/**
 * Servlet implementation class UserController
 */
//@WebServlet("/UserController")
public class HomeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String BOSS = "jsp/bossLogin.jsp";
	private static String STAFF = "jsp/staffLogin.jsp";
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HomeController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String userName = (String) session.getAttribute("userName");
		if(userName != null){
			if(userName.equals("quangpt@gmail.com")){
				ProductImBusiness show = new ProductImBusiness();
				List<Product> listCl = show.showProductClub();
				List<Product> listNa = show.showProductNational();
				List<Product> listSB = show.showProductSB();
				request.setAttribute("employeeList", listCl);
				request.setAttribute("product", listNa);
				request.setAttribute("products", listSB);
				request.getRequestDispatcher(BOSS).forward(request, response);
			}else{
				request.getRequestDispatcher(STAFF).forward(request, response);
			}
		}else{
			//RequestDispatcher view = request.getRequestDispatcher("jsp/homePage.jsp");
	        //view.forward(request, response);
			ProductImBusiness show = new ProductImBusiness();
			List<Product> listCl = show.showProductClub();
			List<Product> listNa = show.showProductNational();
			List<Product> listSB = show.showProductSB();
			request.setAttribute("employeeList", listCl);
			request.setAttribute("product", listNa);
			request.setAttribute("products", listSB);
			RequestDispatcher view = request.getRequestDispatcher("jsp/product.jsp");
		    view.forward(request, response);
		}
        
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
}
}
