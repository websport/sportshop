package com.pnv.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import com.mysql.jdbc.StringUtils;

import com.oreilly.servlet.MultipartRequest;
import com.pnv.business.*;
import com.pnv.dao.ProductImDao;
import com.pnv.model.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ProductController
 */
// @WebServlet("/productController")
public class ProductController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String SHOW_PRODUCT = "jsp/showProduct.jsp";
	private static String BOSS = "jsp/bossLogin.jsp";
	private static String INFORMATION = "jsp/informationProduct.jsp";
	private static String HOME = "jsp/product.jsp";
	private static String SEARCH = "jsp/searchProduct.jsp";
	private static String SHOW_ORDER = "jsp/showOrder.jsp";
	ArrayList<Product> data = new ArrayList<>();
	ProductBusiness search = new ProductImBusiness();
	private static String PRODUCT_MENU = "jsp/productMenu.jsp";
	private static String PRODUCT_INSERT = "jsp/insertProduct.jsp";
	private static String EDIT_PRODUCT = "jsp/editProduct.jsp";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProductController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		// response.getWriter().append("Served at:
		// ").append(request.getContextPath());

		String formAction = request.getParameter("formAction");
		if (formAction.equals("showProduct")) {
			ProductImBusiness show = new ProductImBusiness();
			ArrayList<Product> showProduct = new ArrayList<>();
			showProduct = show.showProduct();

			request.setAttribute("delete_pro", showProduct);

			request.getRequestDispatcher(SHOW_PRODUCT).forward(request, response);
		} else if (formAction.equals("showProductArsenal")) {

			String name = request.getParameter("name");
			ProductImBusiness show = new ProductImBusiness();
			List<Product> list = show.showProductClubArsenal(name);
			request.setAttribute("ListArsenal", list);
			RequestDispatcher view = request.getRequestDispatcher(PRODUCT_MENU);
			view.forward(request, response);

		} else if (formAction.equals("deleteProductByID")) {
			int productId = Integer.parseInt(request.getParameter("submit_id"));

			ProductImBusiness dele = new ProductImBusiness();
			boolean result = dele.deleteProductBo(productId);
			if (result) {
				request.setAttribute("MS", "Delete product successfully!!!!");
				ProductImBusiness show = new ProductImBusiness();
				ArrayList<Product> showProduct = new ArrayList<>();
				showProduct = show.showProduct();
				request.setAttribute("delete_pro", showProduct);
				request.getRequestDispatcher(SHOW_PRODUCT).forward(request, response);
			} else {
				request.setAttribute("MS", "Delete product unsuccessfully!!!!");
				ProductImBusiness show = new ProductImBusiness();
				ArrayList<Product> showProduct = new ArrayList<>();
				showProduct = show.showProduct();
				request.setAttribute("delete_pro", showProduct);
				request.getRequestDispatcher(SHOW_PRODUCT).forward(request, response);
			}
		} else if (formAction.equals("showInforProduct")) {
			String id = request.getParameter("id");
			int productId = Integer.parseInt(id);
			ProductImBusiness show = new ProductImBusiness();
			Product showProduct = show.findProduct(productId);
			request.setAttribute("showPr", showProduct);
			request.getRequestDispatcher(INFORMATION).forward(request, response);

		} else if (formAction.equals("sellProduct")) {
			String staffId = request.getParameter("staffId");
			int iStaffId = Integer.parseInt(staffId);
			String orderId = request.getParameter("orderId");
			int iOrderId = Integer.parseInt(orderId);
			ProductImBusiness pro = new ProductImBusiness();
			boolean result = pro.sellProduct(iStaffId, iOrderId,true);
			if (result == true) {
				ProductImBusiness show = new ProductImBusiness();
				List<Order> list = show.showOrder();
				HttpSession session = request.getSession();
				request.setAttribute("order", list);
				request.setAttribute("MS", "Order thành công!!! ");
				request.getRequestDispatcher(SHOW_ORDER).forward(request, response);
			} else {
				ProductImBusiness show = new ProductImBusiness();
				List<Order> list = show.showOrder();
				HttpSession session = request.getSession();
				request.setAttribute("order", list);
				request.setAttribute("MS", "Order không thành công!!! ");
				request.getRequestDispatcher(SHOW_ORDER).forward(request, response);

			}
		} else if (formAction.equals("deleteOrder")) {
			String orderId = request.getParameter("orderId");
			int iOrderId = Integer.parseInt(orderId);

			ProductImBusiness pro = new ProductImBusiness();
			boolean result = pro.deleteOrder(iOrderId);
			if (result == true) {
				ProductImBusiness show = new ProductImBusiness();
				List<Order> list = show.showOrder();
				HttpSession session = request.getSession();
				request.setAttribute("order", list);
				request.setAttribute("MS", "Hủy đơn hàng thành công!!!");
				request.getRequestDispatcher(SHOW_ORDER).forward(request, response);
			} else {
				ProductImBusiness show = new ProductImBusiness();
				List<Order> list = show.showOrder();
				HttpSession session = request.getSession();
				request.setAttribute("order", list);
				request.setAttribute("MS", "Hủy đơn hàng không thành công!!!");
				request.getRequestDispatcher(SHOW_ORDER).forward(request, response);
			}
		}
		else if (formAction.equals("insertProduct")){
			request.getRequestDispatcher(PRODUCT_INSERT).forward(request, response);
		}
		else if(formAction.equals("editProduct")){
			String id = request.getParameter("submit_id");
			int productId = Integer.parseInt(id);
			ProductImBusiness show = new ProductImBusiness();
			Product showProduct = show.findProduct(productId);
			request.setAttribute("edit_pro", showProduct);
			request.getRequestDispatcher(EDIT_PRODUCT).forward(request, response);		
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		String formAction = request.getParameter("formAction");
		String searchproduct = request.getParameter("searchproduct");

		if ("search".equals(formAction)) {
			if (searchproduct == null) {
				ProductImBusiness show = new ProductImBusiness();
				List<Product> listCl = show.showProductClub();
				List<Product> listNa = show.showProductNational();
				List<Product> listSB = show.showProductSB();
				request.setAttribute("employeeList", listCl);
				request.setAttribute("product", listNa);
				request.setAttribute("products", listSB);
				request.getRequestDispatcher(HOME).forward(request, response);
			} else
				data = search.searchProduct(request.getParameter("searchproduct"));
			request.setAttribute("data", data);
			request.getRequestDispatcher(SEARCH).forward(request, response);
			// }else if(formAction.equals("buyProduct")){
			// String product = request.getParameter("productId");
			// int productId = Integer.parseInt(product);
			// String quantity = request.getParameter("quantity");
			// int iQuantity = Integer.parseInt(quantity);
			// String total = request.getParameter("total");
			// int iTotal = Integer.parseInt(total);
			// List<Product> products = new ArrayList<Product>();
			// Product pro = new Product(productId, iQuantity);
			// products.add(pro);
			// String customerName = request.getParameter("customerName");
			// String address = request.getParameter("customerAddress");
			// String phone = request.getParameter("customerPhone");
			// Order order = new Order(iTotal ,customerName, address, phone,
			// products);
			// ProductImBusiness proImp = new ProductImBusiness();
			// proImp.orderProduct(order);

		} else if (formAction.equals("order")) {
			ProductImBusiness show = new ProductImBusiness();
			List<Order> list = show.showOrder();
			HttpSession session = request.getSession();
			int staffId = (int) session.getAttribute("staffId");
			request.setAttribute("order", list);
			request.setAttribute("staffId", staffId);
			request.getRequestDispatcher(SHOW_ORDER).forward(request, response);

		}
		else if(formAction.equals("editProductSuccess")){
			String id = request.getParameter("submit_id");
			int productId = Integer.parseInt(id);
			String namePr = request.getParameter("name");
			System.out.println("===========PRODUCT NAME:  " + namePr);
			String quantit =request.getParameter("quantity");
			int quantity = Integer.parseInt(quantit);
			System.out.println("===========QUANTITY:  " + quantity);
			String date = request.getParameter("date");
			String size = request.getParameter("size");
			String pric = request.getParameter("price");
			float price = Float.parseFloat(pric);
			String description = request.getParameter("description");
			String statu = request.getParameter("status");
			int status = Integer.parseInt(statu);
			String image = request.getParameter("image");
			Product edit=new Product(productId,namePr,quantity,date,size,price,description,image,status);
			ProductImBusiness updateProduct=new ProductImBusiness();
			
			boolean result= updateProduct.updateProduct(productId,namePr,quantity,date,size,price,description,image,status);
			if(result==true){
			ProductImBusiness show = new ProductImBusiness();
			ArrayList<Product> showProduct = new ArrayList<>();
			showProduct = show.showProduct();

			request.setAttribute("delete_pro", showProduct);
            request.setAttribute("MS", "EDIT Succesfully");
			request.getRequestDispatcher(SHOW_PRODUCT).forward(request, response);  
			//chua xong ti tiep tuc.........
			}
			else {
				ProductImBusiness show = new ProductImBusiness();
				ArrayList<Product> showProduct = new ArrayList<>();
				showProduct = show.showProduct();

				request.setAttribute("delete_pro", showProduct);
	            request.setAttribute("MS", "EDIT not Succesfully");
				request.getRequestDispatcher(SHOW_PRODUCT).forward(request, response); 	
			}
			}
	}

}
