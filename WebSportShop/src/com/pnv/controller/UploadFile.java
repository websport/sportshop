package com.pnv.controller;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.pnv.business.ProductImBusiness;
import com.pnv.validation.FormValidation;

/**
 * Servlet implementation class UploadFile
 */

public class UploadFile extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String UPLOAD_DIRECTORY = "C:\\Users\\ha.dinh\\Documents\\sportshop\\WebSportShop\\WebContent\\images";
	private static String PRODUCT_INSERT = "jsp/insertProduct.jsp";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadFile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MultipartRequest m = new MultipartRequest(request, UPLOAD_DIRECTORY + File.separator);		
			String namePr = m.getParameter("product_name");
			String quantit = m.getParameter("quantity");
			
			String date = m.getParameter("date");
			String size = m.getParameter("size_id");
			String pric = m.getParameter("price");
			
			String description = m.getParameter("description");
			String statu = m.getParameter("status");
			int status = Integer.parseInt(statu);
			String errorInput = FormValidation.productNameValidation(namePr);
			String errorInput1 = FormValidation.productQuantityValidation(quantit);
			String errorInput2 = FormValidation.productValidation(pric);
			if (errorInput.equals("") && errorInput1.equals("") &&errorInput2.equals("") ){
				int quantity = Integer.parseInt(quantit);
				float price = Float.parseFloat(pric);
				String fname = null;
				Enumeration files = m.getFileNames();
				while (files.hasMoreElements()) {
					String name = (String) files.nextElement();
					fname = m.getFilesystemName(name);
					String type = m.getContentType(name);
					if (fname.equals("")) {
						request.setAttribute("message", "Sorry! Insert not sucessfully");		
					} else {
						request.setAttribute("message", "Insert product Successfully");
						ProductImBusiness a = new ProductImBusiness();
						a.createProduct(namePr, quantity, date, size, price, description, fname, status);
					}

			}
			
			}else{
				request.setAttribute("message", errorInput + errorInput1+errorInput2);
			}

//			 File uploaded successfully
			request.getRequestDispatcher(PRODUCT_INSERT).forward(request, response);
	}

}
