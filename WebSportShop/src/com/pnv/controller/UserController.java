package com.pnv.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pnv.business.ProductImBusiness;
import com.pnv.business.UserImpBusiness;
import com.pnv.dao.UserImpDao;
import com.pnv.model.Product;
import com.pnv.model.StaffInformation;
import com.pnv.validation.FormValidation;

/**
 * Servlet implementation class UserController
 */
// @WebServlet("/UserController")
public class UserController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static String LOGIN = "jsp/login.jsp";
	private static String REGISTER = "jsp/register.jsp";
	private static String DELETE = "jsp/delete.jsp";
	private static String EDIT = "jsp/edit.jsp";
	private static String CHANGEPASS = "jsp/changePassword.jsp";
	private static String BOSS = "jsp/bossLogin.jsp";
	private static String STAFF = "jsp/staffLogin.jsp";
	private static String HOMEPAGE = "jsp/homePage.jsp";
	private static String SHOW_STAFF = "jsp/showStaff.jsp";
	private static String EDIT_FORM = "jsp/editAccount.jsp";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String formAction = request.getParameter("formAction");

		if (formAction.equals("loginForm")) {
			request.getRequestDispatcher(LOGIN).forward(request, response);
		} else if (formAction.equals("register")) {
			request.getRequestDispatcher(REGISTER).forward(request, response);
		} else if (formAction.equals("delete")) {
			request.getRequestDispatcher(DELETE).forward(request, response);
		} else if (formAction.equals("edit")) {
			request.getRequestDispatcher(EDIT).forward(request, response);
		} else if (formAction.equals("changePassword")) {
			request.getRequestDispatcher(CHANGEPASS).forward(request, response);
		} else if (formAction.equals("show")) {
			UserImpBusiness show = new UserImpBusiness();
			List<StaffInformation> showStaff = new ArrayList<>();
			showStaff = show.showStaff();
			request.setAttribute("user", showStaff);
			request.getRequestDispatcher(SHOW_STAFF).forward(request, response);
		} else if (formAction.equals("editForm")) {
			String id = request.getParameter("idEdit");
			int iId = Integer.parseInt(id);
			UserImpBusiness show = new UserImpBusiness();
			StaffInformation staff = show.findUser(iId);
			request.setAttribute("edit", staff);
			request.getRequestDispatcher(EDIT_FORM).forward(request, response);
		}else if (formAction.equals("deleteSuccess")) {
			String id = request.getParameter("id");
			int iId = Integer.parseInt(id);
			UserImpBusiness delete = new UserImpBusiness();
			boolean result = delete.deleteStaffById(iId);
			if (result) {
				UserImpBusiness show = new UserImpBusiness();
				List<StaffInformation> showStaff = new ArrayList<>();
				showStaff = show.showStaff();
				request.setAttribute("user", showStaff);
				request.setAttribute("MS", "Xóa thành công!!!!");
				request.getRequestDispatcher(SHOW_STAFF).forward(request, response);
			} else {
				UserImpBusiness show = new UserImpBusiness();
				List<StaffInformation> showStaff = new ArrayList<>();
				showStaff = show.showStaff();
				request.setAttribute("user", showStaff);
				request.setAttribute("MS", "Xóa không thành công!!!");
				request.getRequestDispatcher(SHOW_STAFF).forward(request, response);
			}

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");

		String action = request.getParameter("action");

		if (action.equals("login")) {

			String userName = request.getParameter("userName");
			String password = request.getParameter("password");

			UserImpBusiness user = new UserImpBusiness();
			int result = user.checkLogin(userName, password);

			if (result != 0) {
				HttpSession session = request.getSession(false);
				session.setAttribute("userName", userName);
				session.setAttribute("staffId", result);
				if (userName.equals("quangpt@gmail.com")) {
					ProductImBusiness show = new ProductImBusiness();
					List<Product> listCl = show.showProductClub();
					List<Product> listNa = show.showProductNational();
					List<Product> listSB = show.showProductSB();
					request.setAttribute("employeeList", listCl);
					request.setAttribute("product", listNa);
					request.setAttribute("products", listSB);
					request.getRequestDispatcher(BOSS).forward(request, response);
				} else {
					request.getRequestDispatcher(STAFF).forward(request, response);
				}
			} else {
				request.setAttribute("MS", "Không thành công, vui lòng kiểm tra lại!");
				request.getRequestDispatcher(LOGIN).forward(request, response);
			}

		} else if (action.equals("register")) {

			String name = request.getParameter("fullName");
			String address = request.getParameter("address");
			String phone = request.getParameter("phone");
			String gender = request.getParameter("gender");
			String identityCard = request.getParameter("identityCard");
			String userName = request.getParameter("userName");
			String password = request.getParameter("password");
			String role = request.getParameter("role");

			String errorFullNameInput = FormValidation.fullNameValidation(name);
			String errorAddressInput = FormValidation.addressValidation(address);
			String errorPhoneInput = FormValidation.phoneNumberInputValidation(phone);
			String errorIdentityCardInput = FormValidation.identityCardInputValidation(identityCard);
			String errorUserNameInput = FormValidation.emailInputValidation(userName);
			String errorPasswordInput = FormValidation.passwordValidation(password);

			if (errorFullNameInput.equals("") &&errorAddressInput.equals("") && errorPhoneInput.equals("") && errorIdentityCardInput.equals("")
					&& errorUserNameInput.equals("") && errorPasswordInput.equals("") && role.equals("")) {
				int iIdentityCard = Integer.parseInt(identityCard);
				int iRole = Integer.parseInt(role);
				UserImpBusiness user = new UserImpBusiness();

				boolean result;

				if (gender.equals("1")) {
					result = user.createNewStaff(name, address, phone, true, iIdentityCard, userName, password, iRole);
				} else {
					result = user.createNewStaff(name, address, phone, false, iIdentityCard, userName, password, iRole);
				}
				if (result) {
					request.setAttribute("MS", "Chúc mừng đã đăng kí thành công !!!!");
					request.getRequestDispatcher(BOSS).forward(request, response);
				} else {
					request.setAttribute("MS", "Đăng kí không thành công, vui lòng kiểm tra lại thông tin!!!!!");
					request.getRequestDispatcher(REGISTER).forward(request, response);
				}
			} else {
				request.setAttribute("MS", errorFullNameInput +errorAddressInput+ errorPhoneInput + errorIdentityCardInput
						+ errorUserNameInput + errorPasswordInput);
				request.getRequestDispatcher(REGISTER).forward(request, response);
			}

		} else if (action.equals("edit")) {
			String id = request.getParameter("idEdit");
			int iId = Integer.parseInt(id);

			String address = request.getParameter("address");
			String phone = request.getParameter("phone");

			String userName = request.getParameter("userName");
			String password = request.getParameter("password");

			String errorAddressInput = FormValidation.addressValidation(address);
			String errorPhoneInput = FormValidation.phoneNumberInputValidation(phone);
			String errorUserNameInput = FormValidation.emailInputValidation(userName);
			String errorPasswordInput = FormValidation.passwordValidation(password);
			
			if (errorPhoneInput.equals("") && errorAddressInput.equals("") && errorUserNameInput.equals("")
					&& errorPasswordInput.equals("")) {
				UserImpBusiness change = new UserImpBusiness();
				boolean result = change.changeInformationOfStaff(iId, address, phone, userName, password);
				if (result) {
					UserImpBusiness show = new UserImpBusiness();
					List<StaffInformation> showStaff = new ArrayList<>();
					showStaff = show.showStaff();
					request.setAttribute("user", showStaff);
					request.setAttribute("MS", "Chỉnh sửa thành công!!!");
					request.getRequestDispatcher(SHOW_STAFF).forward(request, response);
				} else {
					UserImpBusiness show = new UserImpBusiness();
					StaffInformation staff = show.findUser(iId);
					request.setAttribute("edit", staff);
					request.setAttribute("MS", "Chỉnh sửa thất bại!!!");
					request.getRequestDispatcher(EDIT_FORM).forward(request, response);
				}
			} else {
				UserImpBusiness show = new UserImpBusiness();
				StaffInformation staff = show.findUser(iId);
				request.setAttribute("edit", staff);
				request.setAttribute("MS",
				errorPhoneInput + errorAddressInput + errorUserNameInput + errorPasswordInput);
				request.getRequestDispatcher(EDIT_FORM).forward(request, response);
			}

		} else if (action.equals("changePassword")) {
			HttpSession session = request.getSession();
			String userName = (String) session.getAttribute("userName");
			String password = request.getParameter("pass");
			String newPassword = request.getParameter("newPassword");
			String confirmPassword = request.getParameter("confirmPassword");
			UserImpBusiness user = new UserImpBusiness();
			if (newPassword.equals(confirmPassword)) {
				boolean result = user.changePassword(userName, password, newPassword);
				if (result) {
					if (userName.equals("quangpt@gmail.com")) {
						request.setAttribute("MS", "Đổi mật khẩu thành công!!!!");
						request.getRequestDispatcher(BOSS).forward(request, response);
					} else {
						request.setAttribute("MS", "Đổi mật khẩu thành công!!!!");
						request.getRequestDispatcher(STAFF).forward(request, response);
					}
				} else {
					request.setAttribute("MS", "Mật khẩu cũ không đúng!!");
					request.getRequestDispatcher(CHANGEPASS).forward(request, response);
				}
			} else {
				request.setAttribute("MS", "Mật khẩu mới nhập không khớp !!!");
				request.getRequestDispatcher(CHANGEPASS).forward(request, response);
			}
		} else if (action.equals("logout")) {
			HttpSession session = request.getSession();
			session.invalidate();
			//request.getRequestDispatcher(HOMEPAGE).forward(request, response);
			ProductImBusiness show = new ProductImBusiness();
			List<Product> listCl = show.showProductClub();
			List<Product> listNa = show.showProductNational();
			List<Product> listSB = show.showProductSB();
			request.setAttribute("employeeList", listCl);
			request.setAttribute("product", listNa);
			request.setAttribute("products", listSB);
			RequestDispatcher view = request.getRequestDispatcher("jsp/product.jsp");
		    view.forward(request, response);
		}

	}

}
