package com.pnv.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.pnv.model.Order;
import com.pnv.model.Product;
import com.pnv.model.ProductOrder;

public interface ProductDao {

	List<Product> showProductClub();
	List<Product> showProductClubArsenal(String nameClub);
	List<Product> showProductSB();
	List<Product> showProductNational();
	List<Product> showProductDAO();
	boolean deleteProduct(int productId);
	
    
	Product showProduct(int productId);
	ArrayList<Product> searching(String values) ;
//	boolean insertOrderProduct(Order order);
	List<Order> showOrder();
	List<ProductOrder> showProductOrder(int order_id);
	boolean sellProduct(int staffId, int orderId, boolean status);
	boolean deleteOrder(int orderId);
	boolean createProduct(Product product);
	boolean deleteProductOrder(int orderId);
	boolean editProduct(int productId, String namePr, int quantity, String date, String size, float price,
			String description, String image, int status);
}
