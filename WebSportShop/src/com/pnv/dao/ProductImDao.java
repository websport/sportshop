package com.pnv.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;

import com.pnv.model.Order;
import com.pnv.model.Product;
import com.pnv.model.ProductOrder;

import com.pnv.utils.DbHelper;

public class ProductImDao implements ProductDao {

	@Override
	public List<Product> showProductClubArsenal(String nameClub) {
		Connection connection = DbHelper.returnDbHelper();
		List<Product> list = new ArrayList<>();
		String showProduct = "Select product_id,product_name,quantity,date_update,size_id,price,description,image From product where product_name=?";
		try {
			PreparedStatement ps = connection.prepareStatement(showProduct);
			ps.setString(1, nameClub);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				int productId = rs.getInt(1);
				String productName = rs.getString(2);
				int quantity = rs.getInt(3);
				String dateUpdate = rs.getString(4);
				String sizeId = rs.getString(5);
				int price = rs.getInt(6);
				String description = rs.getString(7);
				String image = rs.getString(8);
				Product product = new Product(productId, productName, quantity, dateUpdate, sizeId, price, description,
						image);
				list.add(product);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return list;
	}

	@Override
	public List<Product> showProductClub() {
		Connection connection = DbHelper.returnDbHelper();
		ArrayList<Product> list = new ArrayList<>();
		String dele = "SELECT product_id,product_name, quantity, date_update, size_id, price, description,image,status FROM product where status='0' ORDER BY RAND()";
		try {
			PreparedStatement pstm = connection.prepareStatement(dele);
			ResultSet rs = pstm.executeQuery();

			while (rs.next()) {
				int productId = rs.getInt(1);
				String productName = rs.getString(2);
				int quantity = rs.getInt(3);
				String dateUpdate = rs.getString(4);
				String sizeId = rs.getString(5);
				int price = rs.getInt(6);
				String description = rs.getString(7);
				String image = rs.getString(8);
				Product product = new Product(productId, productName, quantity, dateUpdate, sizeId, price, description,
						image);
				list.add(product);
			}
			connection.close();
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ArrayList<Product> showProductDAO() {
		// TODO Auto-generated method stub
		Connection connection = DbHelper.returnDbHelper();
		ArrayList<Product> list = new ArrayList<>();
		String dele = "SELECT product_id,product_name, quantity, date_update, size_id, price, description FROM product";
		try {
			PreparedStatement pstm = connection.prepareStatement(dele);
			ResultSet rs = pstm.executeQuery();

			while (rs.next()) {
				int productId = rs.getInt(1);
				String productName = rs.getString(2);
				int quantity = rs.getInt(3);
				String dateUpdate = rs.getString(4);
				String sizeId = rs.getString(5);
				int price = rs.getInt(6);
				String description = rs.getString(7);
				Product product = new Product(productId, productName, quantity, dateUpdate, sizeId, price, description);
				list.add(product);
			}
			connection.close();
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean deleteProduct(int productId) {
		// TODO Auto-generated method stub
		Connection connection = DbHelper.returnDbHelper();
		String delete = "DELETE FROM product WHERE product_id=?";
		String getname = "SELECT image FROM product WHERE product_id=?";

		try {
			PreparedStatement pre1 = connection.prepareStatement(getname);
			pre1.setInt(1, productId);
			ResultSet rs = pre1.executeQuery();
			if (rs.next()) {
				String url = "C:/Users/nhan.ly/project/WebSportShop/WebContent/images/" + rs.getString(1);
				boolean success = (new File(url)).delete();
				PreparedStatement pre = connection.prepareStatement(delete);
				pre.setInt(1, productId);
				int de = pre.executeUpdate();

				if (de == 1 && success == true) {
					return true;
				}

			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Product> showProductNational() {
		Connection connection = DbHelper.returnDbHelper();
		ArrayList<Product> list = new ArrayList<>();
		String dele = "SELECT product_id,product_name, quantity, date_update, size_id, price, description,image,status FROM product where status='1' ORDER BY RAND()";
		try {
			PreparedStatement pstm = connection.prepareStatement(dele);
			ResultSet rs = pstm.executeQuery();

			while (rs.next()) {
				int productId = rs.getInt(1);
				String productName = rs.getString(2);
				int quantity = rs.getInt(3);
				String dateUpdate = rs.getString(4);
				String sizeId = rs.getString(5);
				int price = rs.getInt(6);
				String description = rs.getString(7);
				String image = rs.getString(8);
				Product product = new Product(productId, productName, quantity, dateUpdate, sizeId, price, description,
						image);
				list.add(product);
			}
			connection.close();
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Product> showProductSB() {
		Connection connection = DbHelper.returnDbHelper();
		ArrayList<Product> list = new ArrayList<>();
		String dele = "SELECT product_id,product_name, quantity, date_update, size_id, price, description,image,status FROM product where status='2' ORDER BY RAND()";
		try {
			PreparedStatement pstm = connection.prepareStatement(dele);
			ResultSet rs = pstm.executeQuery();

			while (rs.next()) {
				int productId = rs.getInt(1);
				String productName = rs.getString(2);
				int quantity = rs.getInt(3);
				String dateUpdate = rs.getString(4);
				String sizeId = rs.getString(5);
				int price = rs.getInt(6);
				String description = rs.getString(7);
				String image = rs.getString(8);
				Product product = new Product(productId, productName, quantity, dateUpdate, sizeId, price, description,
						image);
				list.add(product);
			}
			connection.close();
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // TODO Auto-generated method stub
		return null;
	}

	@Override
	public Product showProduct(int productId) {
		Connection connection = DbHelper.returnDbHelper();
		String dele = "SELECT product_id,product_name, quantity, date_update,"
				+ " size_id, price, description,image,status FROM product where product_id=?";
		try {
			PreparedStatement pstm = connection.prepareStatement(dele);
			pstm.setInt(1, productId);
			ResultSet rs = pstm.executeQuery();

			while (rs.next()) {
				int Id = rs.getInt(1);
				String productName = rs.getString(2);
				int quantity = rs.getInt(3);
				String dateUpdate = rs.getString(4);
				String sizeId = rs.getString(5);
				int price = rs.getInt(6);
				String description = rs.getString(7);
				String image = rs.getString(8);
				Product product = new Product(Id, productName, quantity, dateUpdate, sizeId, price, description, image);
				return product;
			}
			connection.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Product> searching(String values) {
		Connection connection = DbHelper.returnDbHelper();
		String sql = "select product_id,price,product_name from product where match(description) Against('" + values
				+ "') OR description LIKE '%" + values + "' OR description like '" + values + "%';";
		ArrayList<Product> list = new ArrayList<>();
		try {
			PreparedStatement pre = connection.prepareStatement(sql);

			ResultSet rs = pre.executeQuery();

			String x = null;
			while (rs.next()) {
				Product p = new Product();
				p.setProductId(rs.getInt("product_id"));

				p.setProductName(rs.getString("product_name").concat(rs.getString("product_id")));

				p.setPrice(rs.getFloat("price"));

				list.add(p);

			}
			connection.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
			return null;
		}
		return list;
	}


	@Override
	public boolean createProduct(Product product) {
		Connection connection = DbHelper.returnDbHelper();
		String creatProduct="INSERT INTO product (product_name,quantity,date_update,size_id,price,description,image,status) Values (?,?,?,?,?,?,?,?)";

		try {
			PreparedStatement pre = connection.prepareStatement(creatProduct);
			pre.setString(1, product.getProductName());
			pre.setInt(2, product.getQuantity());
			pre.setString(3, product.getDateUpdate());
			pre.setString(4, product.getSizeId());
			pre.setFloat(5, product.getPrice());
			pre.setString(6, product.getDescription());
			pre.setString(7, product.getImage());
			pre.setInt(8, product.getStatus());
			int notification = pre.executeUpdate();
			if (notification == 1) {
				return true;
			}
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return false;
	}

	

	// @Override
	// public boolean insertOrderProduct(Order order) {
	// Connection connection = DbHelper.returnDbHelper();
	// PreparedStatement preInsertOrder = null;
	// PreparedStatement preInsertPro = null;
	// String insertOrder = "INSERT INTO
	// order(date_order,status,total,staff_id,customer_name,address,phone)"
	// + "VALUES(?,?,?,?,?,?,?,?,?)";
	// String insertProductOrder = "INSERT INTO
	// product_order(order_id,product_id,quantity) VALUES (?,?,?)";
	// try {
	// connection.setAutoCommit(false);
	// preInsertOrder = connection.prepareStatement(insertOrder);
	// preInsertPro = connection.prepareStatement(insertProductOrder,
	// Statement.RETURN_GENERATED_KEYS);
	// String currentDay;
	// Format formatter;
	// Date date = new Date();
	// formatter = new SimpleDateFormat("yyyy-MM-dd");
	// currentDay = formatter.format(date);
	//
	// preInsertOrder.setString(1, currentDay);
	// preInsertOrder.setBoolean(2, order.isStatus());
	// preInsertOrder.setDouble(3, order.getTotal());
	// preInsertOrder.setInt(4, order.getStaffId());
	// preInsertOrder.setString(5, order.getCustomerName());
	// preInsertOrder.setString(6, order.getAddress());
	// preInsertOrder.setString(7, order.getPhone());
	//
	// preInsertOrder.executeUpdate();
	// ResultSet keys = preInsertOrder.getGeneratedKeys();
	// keys.next();
	// int key = keys.getInt(1);
	//
	// for (int i = 0; i <= order.getProducts().size(); i++) {
	// preInsertPro.setInt(1, key);
	// preInsertPro.setInt(2, order.getProducts().get(i).getProductId());
	// preInsertPro.setInt(3, order.getProducts().get(i).getQuantity());
	// preInsertPro.addBatch();
	// }
	// preInsertPro.executeBatch();
	// connection.commit();
	// keys.close();
	// preInsertOrder.close();
	// preInsertPro.close();
	// connection.close();
	// return true;
	//
	// } catch (SQLException e) {
	// if (connection != null) {
	// try {
	// connection.rollback();
	// } catch (SQLException excep) {
	//
	// }
	// }
	// }
	// return false;
	// }

	@Override
	public List<Order> showOrder() {
		Connection connection = DbHelper.returnDbHelper();
		String selectOrder = "SELECT * FROM order_customer";
		List<Order> order = new ArrayList<Order>();
		try {
			PreparedStatement pre = connection.prepareStatement(selectOrder);
			ResultSet list = pre.executeQuery();
			while (list.next()) {
				Order allOrder = new Order(list.getInt(1), list.getDate(2), list.getBoolean(3), list.getDouble(4),
						list.getInt(5), list.getString(6), list.getString(7), list.getString(8));
				order.add(allOrder);

			}
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return order;

	}

	@Override
	public List<ProductOrder> showProductOrder(int order_id) {
		Connection connection = DbHelper.returnDbHelper();
		String selectProductOrder = "SELECT order_id, product_id, quantity FROM product_order WHERE order_id = ?";
		List<ProductOrder> pro = new ArrayList<ProductOrder>();
		try {
			PreparedStatement pre = connection.prepareStatement(selectProductOrder);
			pre.setInt(1, order_id);
			ResultSet list = pre.executeQuery();
			while (list.next()) {
				ProductOrder allProOrder = new ProductOrder(list.getInt(1), list.getInt(2), list.getInt(3));
				pro.add(allProOrder);
			}
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return pro;
	}

	@Override
	public boolean sellProduct(int staffId, int orderId, boolean status) {
		Connection connection = DbHelper.returnDbHelper();
		String updateStaffId = "Update order_customer SET staff_id = ?, status = ? WHERE order_id = ?";
		try {
			PreparedStatement pre = connection.prepareStatement(updateStaffId);
			pre.setInt(1, staffId);
			pre.setBoolean(2, status);
			pre.setInt(3, orderId);
			int notification = pre.executeUpdate();
			if (notification == 1) {
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteOrder(int orderId) {
		Connection connection = DbHelper.returnDbHelper();
		String deletePro = "DELETE FROM product_order WHERE order_id = ?";
		String deleteCus = "DELETE FROM order_customer WHERE order_id = ?";
		
		try {
			connection.setAutoCommit(false);
			PreparedStatement deleteProduct = connection.prepareStatement(deletePro);
			PreparedStatement deleteCustomer = connection.prepareStatement(deleteCus);
			deleteProduct.setInt(1, orderId);
			deleteProduct.executeUpdate();
			deleteCustomer.setInt(1, orderId);
			int notification = deleteCustomer.executeUpdate();
			connection.commit();
			connection.close();
			if (notification == 1) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;

	}

	@Override
	public boolean deleteProductOrder(int orderId) {
//		Connection connection = DbHelper.returnDbHelper();
//		String deletePro = "DELETE FROM product_order WHERE order_id = ?";
		return false;
	}

	@Override
	public boolean editProduct(int productId, String namePr, int quantity, String date, String size, float price,
			String description, String image, int status) {
		 Connection connection = DbHelper.returnDbHelper();
		 String sql = "Update product set product_id=?, product_name=?,quantity=?,date_update=?,size_id=?,price=?,description=?,image=?,status=? where product_id=? ";
		 Product product =new Product(productId,namePr,quantity,date,size,price,description,image,status);
		 try {
	      PreparedStatement pstm = connection.prepareStatement(sql);
	      
	      pstm.setInt(1, product.getProductId());
	      pstm.setString (2, product.getProductName());
	      pstm.setInt (3, product.getQuantity());
	      pstm.setString(4, product.getDateUpdate());
	      pstm.setString(5, product.getSizeId());
	      pstm.setFloat(6, product.getPrice());  
	      pstm.setString(7, product.getDescription());  
	      pstm.setString(8, product.getImage());  
	      pstm.setInt(9, product.getStatus());  
	      pstm.setInt(10, product.getProductId());     
	      pstm.executeUpdate();// TODO Auto-generated method stub
	} catch (Exception e) {
		// TODO: handle exception
	}
		return false;
	}

}
