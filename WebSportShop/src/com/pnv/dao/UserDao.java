package com.pnv.dao;

import java.util.List;

import com.pnv.model.StaffInformation;

public interface UserDao {
	
	boolean createNewStaff(StaffInformation staffInformation);
	boolean editStaffInformationById(StaffInformation staffInformation);
	boolean deleteStaffById(int id);
	int selectStaffId(String userName, String password);
	boolean updatePassword(String userName, String password, String newPassword);
	List<StaffInformation> queryStaff();
	StaffInformation showStaff(int staffId);
	boolean updateInformationOfStaff(int id, String address, String phone, String userName, String password);
}
