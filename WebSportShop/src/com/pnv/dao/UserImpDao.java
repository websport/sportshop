package com.pnv.dao;

import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;
import com.pnv.model.StaffInformation;
import com.pnv.utils.DbHelper;

public class UserImpDao implements UserDao {

	@Override
	public boolean createNewStaff(StaffInformation staffInformation) {

		Connection connection = DbHelper.returnDbHelper();
		String createNewStaff = "INSERT INTO staff_information (name, address, phone,"
				+ " gender, identity_card, username, password, role_id, description_password) VALUES (?,?,?,?,?,?,MD5(?),?,?)";

		try {
			PreparedStatement pre = connection.prepareStatement(createNewStaff);
			pre.setString(1, staffInformation.getName());
			pre.setString(2, staffInformation.getAddress());
			pre.setString(3, staffInformation.getPhone());
			pre.setBoolean(4, staffInformation.isGender());
			pre.setInt(5, staffInformation.getIdentityCard());
			pre.setString(6, staffInformation.getUserName());
			pre.setString(7, staffInformation.getPassword());
			pre.setInt(8, staffInformation.getRole());
			pre.setString(9, staffInformation.getPassword());
			int notification = pre.executeUpdate();
			if (notification == 1) {
				return true;
			}
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
				return false;
	}

	@Override
	public boolean deleteStaffById(int id) {
			Connection connection = DbHelper.returnDbHelper();
			String deleteStaff = "DELETE FROM staff_information WHERE staff_id= ?";
			try {
				PreparedStatement pre = connection.prepareStatement(deleteStaff);
				pre.setInt(1, id);
				int notification = pre.executeUpdate();
				if (notification == 1) {
					return true;
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		return false;
	}

	@Override
	public int selectStaffId(String userName, String password){
		Connection connection = DbHelper.returnDbHelper();
		String checkLogin = "SELECT staff_id FROM staff_information WHERE username = ? and password = ?";
		try {
			PreparedStatement pre = connection.prepareStatement(checkLogin);
			pre.setString(1,userName);
			pre.setString(2,password);
			ResultSet resultSet = pre.executeQuery();
			if(resultSet.next()) {
				int staffId = resultSet.getInt(1);
				return staffId;
			}
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
				return 0;
	}

	@Override
	public boolean editStaffInformationById(StaffInformation staffInformation) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updatePassword(String userName, String password, String newPassword) {
		Connection connection = DbHelper.returnDbHelper();
		String changePassword = "UPDATE staff_information SET password = MD5(?),"
				+ "description_password = ? WHERE username = ? AND password = MD5(?)";
		try {
			PreparedStatement pre = connection.prepareStatement(changePassword);
			pre.setString(1,newPassword);
			pre.setString(2,newPassword);
			pre.setString(3,userName);
			pre.setString(4,password);
			int notification = pre.executeUpdate();
			if(notification == 1){
				return true;
			}
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public List<StaffInformation> queryStaff() {
		Connection connection = DbHelper.returnDbHelper();
		List<StaffInformation> list = new ArrayList<>();
		String select = "Select staff_id, name, address, phone,gender,identity_card,"
				+ "username,description_password,role_id From staff_information";

		try {
			PreparedStatement pstm = connection.prepareStatement(select, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = pstm.executeQuery();
			
			while (rs.next()) {
				int staffId = rs.getInt(1);
				String name = rs.getString(2);
				String address = rs.getString(3);
				String phone = rs.getString(4);
				boolean gender = rs.getBoolean(5);
				int identityCard = rs.getInt(6);
				String userName = rs.getString(7);
				String password = rs.getString(8);
				int roleId = rs.getInt(9);
				StaffInformation staff = new StaffInformation(staffId, name, address, phone, gender, identityCard, userName, password, roleId);
				list.add(staff);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public StaffInformation showStaff(int staffId) {
			Connection connection = DbHelper.returnDbHelper();
			String select = "Select staff_id, name, address, phone,"
					+ "gender,identity_card,username,description_password,role_id From staff_information Where staff_id=?";
       try {
			PreparedStatement pstm = connection.prepareStatement(select);
			pstm.setInt(1, staffId);
			ResultSet rs = pstm.executeQuery();
			if (rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				String address = rs.getString(3);
				String phone = rs.getString(4);
				boolean gender = rs.getBoolean(5);
				int identityCard = rs.getInt(6);
				String userName = rs.getString(7);
				String password = rs.getString(8);
				int roleId = rs.getInt(9);
				
				StaffInformation staff = new StaffInformation(id,name, address, phone, gender, 
						identityCard, userName, password, roleId);
				return staff;
			}
			}catch (Exception e) {
				// TODO: handle exception
			}
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean updateInformationOfStaff(int id, String address, String phone, String userName, String password) {
		Connection connection = DbHelper.returnDbHelper();
		String sql = "UPDATE staff_information SET address = ?, phone = ?, username= ?, password= MD5(?), description_password = ?"
				+ "WHERE staff_id = ?";
		try {
			PreparedStatement pre = connection.prepareStatement(sql);
			pre.setString(1,address);
			pre.setString(2,phone);
			pre.setString(3,userName);
			pre.setString(4,password);
			pre.setString(5,password);
			pre.setInt(6,id);
			int notification = pre.executeUpdate();
			if(notification == 1){
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}
