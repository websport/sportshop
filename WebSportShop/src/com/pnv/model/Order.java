package com.pnv.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Order {
	private int orderId;
	private Date orderDate;
	private boolean status = false;
	private double total;
	private int staffId = 1;
	private String customerName;
	private String address;
	private String phone;
	private List<ProductOrder> products = new ArrayList<ProductOrder>();
	
	
	public Order(int orderId, Date orderDate, boolean status, double total, int staffId, String customerName,
			String address, String phone) {
		super();
		this.orderId = orderId;
		this.orderDate = orderDate;
		this.status = status;
		this.total = total;
		this.staffId = staffId;
		this.customerName = customerName;
		this.address = address;
		this.phone = phone;
	}

	
	public Order(int orderId, Date orderDate, boolean status, double total, int staffId, String customerName,
			String address, String phone, List<ProductOrder> products) {
		super();
		this.orderId = orderId;
		this.orderDate = orderDate;
		this.status = status;
		this.total = total;
		this.staffId = staffId;
		this.customerName = customerName;
		this.address = address;
		this.phone = phone;
		this.products = products;
	}


	public List<ProductOrder> getProducts() {
		return products;
	}


	public void setProducts(List<ProductOrder> products) {
		this.products = products;
	}


	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public int getStaffId() {
		return staffId;
	}
	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
}
