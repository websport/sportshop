package com.pnv.model;

public class Product {
	
	private int productId;
	private String productName;
	private int quantity;
	private String dateUpdate;
	private String sizeId;
	private float price;
	private String description;
	private String image;
	private int status;
	
	
	public Product(int productId, int quantity) {
		super();
		this.productId = productId;
		this.quantity = quantity;
	}


	public Product(int productId, String productName, int quantity, String dateUpdate,String sizeId, float price,
			String description) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.quantity = quantity;
		this.dateUpdate = dateUpdate;
		this.sizeId = sizeId;
		this.price = price;
		this.description = description;
	}

	
	public Product(int productId, String productName, int quantity, String dateUpdate, String sizeId, float price,
			String description, String image) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.quantity = quantity;
		this.dateUpdate = dateUpdate;
		this.sizeId = sizeId;
		this.price = price;
		this.description = description;
		this.image = image;
	}

	public Product(String productName, int quantity, String dateUpdate, String sizeId, float price, String description,
			String image, int status) {
		super();
		this.productName = productName;
		this.quantity = quantity;
		this.dateUpdate = dateUpdate;
		this.sizeId = sizeId;
		this.price = price;
		this.description = description;
		this.image = image;
		this.status = status;
	}


	public Product(int productId, String productName, int quantity, String dateUpdate, String sizeId, float price,
			String description, String image, int status) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.quantity = quantity;
		this.dateUpdate = dateUpdate;
		this.sizeId = sizeId;
		this.price = price;
		this.description = description;
		this.image = image;
		this.status = status;
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public Product() {
	}


	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(String dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	public String getSizeId() {
		return sizeId;
	}

	public void setSizeId(String sizeId) {
		this.sizeId = sizeId;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
	
	
}
	