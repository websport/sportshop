package com.pnv.model;

public class Size {
	
	private int sizeId;
	private String typeOfSize;
	
	public Size(int sizeId, String typeOfSize) {
		super();
		this.sizeId = sizeId;
		this.typeOfSize = typeOfSize;
	}

	public int getSizeId() {
		return sizeId;
	}

	public void setSizeId(int sizeId) {
		this.sizeId = sizeId;
	}

	public String getTypeOfSize() {
		return typeOfSize;
	}

	public void setTypeOfSize(String typeOfSize) {
		this.typeOfSize = typeOfSize;
	}
	
	
}
