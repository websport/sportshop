package com.pnv.model;

public class StaffInformation {
	
	private int id;
	private String name ;
	private String address;
	private String phone;
	private boolean gender;
	private int identityCard;
	private String userName;
	private String password;
	private int role;
	
	
	public StaffInformation(String name, String address, String phone, boolean gender, int identityCard,
			String userName, String password, int role) {
		super();
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.gender = gender;
		this.identityCard = identityCard;
		this.userName = userName;
		this.password = password;
		this.role = role;
	}

	public StaffInformation(int id, String name, String address, String phone, boolean gender, int identityCard,
			String userName, String password, int role) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.gender = gender;
		this.identityCard = identityCard;
		this.userName = userName;
		this.password = password;
		this.role = role;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}


	public int getIdentityCard() {
		return identityCard;
	}



	public void setIdentityCard(int identityCard) {
		this.identityCard = identityCard;
	}


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}



}
