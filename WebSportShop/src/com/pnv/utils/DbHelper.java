package com.pnv.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;


public class DbHelper {

	private static String URL = "jdbc:mysql://localhost:3306/database?useUnicode=true&characterEncoding=UTF-8";
	private static String USER = "root";
	private static String PASSWORD = "";
	private static String DRIVER = "com.mysql.jdbc.Driver";

	public DbHelper() {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Connection returnDbHelper() {
		new DbHelper();
		try {
			Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
			return connection;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
